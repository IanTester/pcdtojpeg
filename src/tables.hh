/* =======================================================
 * pcdDecode - a Photo-CD image decoder and converter
 * =======================================================
 *
 * Project Info:  http://sourceforge.net/projects/pcdtojpeg/
 * Project Lead:  Sandy McGuffog (sandy.cornerfix@gmail.com);
 *
 * (C) Copyright 2009-2011, by Sandy McGuffog and Contributors.
 *
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * ---------------
 * See pcdDecode.hh for more information
 */

#pragma once

#include <string>
#include <map>
#include <vector>
#include "types.hh"

//////////////////////////////////////////////////////////////
//
// Definition Tables - these control most aspects of decoder
// behaviour
//
//////////////////////////////////////////////////////////////

#define kSceneSectorSize KSectorSize

struct PCDSceneMetadata {
  std::string name;
  uint32_t lumaWidth, lumaHeight;
  uint32_t chromaWidth, chromaHeight;
  uint32_t rowShift, rowMask, rowSubsample;
  uint32_t sequenceShift, sequenceMask;
  uint32_t planeShift, planeMask;
  uint32_t HuffmanHeaderSize;
};

extern const std::map<PCDResolution, PCDSceneMetadata> PCDScenes;

//////////////////////////////////////////////////////////////
//
// Lookups for metadata, film code definitions, etc
//
//////////////////////////////////////////////////////////////

extern const std::vector<std::string> PCDMediumTypes;
extern const std::vector<std::string> PCDSBATypes;
extern const std::vector<std::string> PCDHuffmanClasses;
extern const std::map<uint16_t, PCDFilmTermData> PCDFTN_PC_GC_Medium;
extern const std::map<PCDMetaDataDictionary, std::string> PCDMetadataDescriptions;
