/*
        Copyright 2017 Ian Tester

        This file is part of pcdtojpeg.

        pcdtojpeg is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        pcdtojpeg is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with pcdtojpeg.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <unordered_map>
#include <string>

#include <omp.h>
#include <glib/gi18n.h>
#include <libgimp/gimp.h>
#include <libgimp/gimpui.h>
#include <libgimpwidgets/gimpwidgets.h>

#include "pcdDecode.hh"

#define LOAD_PROC       "file-pcd-load"
#define LOAD_THUMB_PROC "file-pcd-load-thumb"
#define PLUG_IN_BINARY  "file-pcd"
#define PLUG_IN_ROLE    "gimp-file-pcd"

struct PCDLoadVals {
  gboolean isD50White;
  gfloat boost;
  gint resolution;
};

static PCDLoadVals plvals = { false, 0, static_cast<gint>(PCDResolution::Base_x16) };

void check_load_vals(void) {
  if (plvals.boost < -0.5)
    plvals.boost = -0.5;
  else if (plvals.boost > 7)
    plvals.boost = 7;

  if (plvals.resolution < 0)
    plvals.resolution = 0;
  else if (plvals.resolution > static_cast<gint>(PCDResolution::Base_x64))
    plvals.resolution = static_cast<gint>(PCDResolution::Base_x64);
}

void cb_boost(GtkAdjustment *adj) {
  plvals.boost = adj->value;
}

gboolean load_dialog(PCDResolution file_res) {
  gimp_ui_init (PLUG_IN_BINARY, FALSE);

  GtkWidget *dialog = gimp_dialog_new(_("Load Photo CD file"), PLUG_IN_ROLE,
				      NULL, (GtkDialogFlags)0,
				      gimp_standard_help_func, LOAD_PROC,

				      _("_Cancel"), GTK_RESPONSE_CANCEL,
				      _("_Open"),   GTK_RESPONSE_OK,

				      NULL);

  gtk_dialog_set_alternative_button_order(GTK_DIALOG (dialog),
					  GTK_RESPONSE_OK,
					  GTK_RESPONSE_CANCEL,
					  -1);

  gtk_window_set_resizable(GTK_WINDOW(dialog), FALSE);

  gimp_window_set_transient(GTK_WINDOW(dialog));

  GtkWidget *vbox = gtk_vbox_new(FALSE, 12);
  gtk_container_set_border_width(GTK_CONTAINER(vbox), 12);
  gtk_box_pack_start(GTK_BOX(gtk_dialog_get_content_area(GTK_DIALOG (dialog))),
		     vbox, TRUE, TRUE, 0);
  gtk_widget_show(vbox);

  {
    GtkWidget *frame = gimp_int_radio_group_new(TRUE, _("White point"),
						G_CALLBACK(gimp_radio_button_update),
						&plvals.isD50White, plvals.isD50White,

						_("D65"), FALSE, NULL,
						_("D50"), TRUE,  NULL,

						NULL);
    gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, FALSE, 0);
    gtk_widget_show(frame);
  }

  {
    GtkWidget *frame = gtk_vbox_new(FALSE, 2);

    GtkWidget *label = gtk_label_new(_("Brightness boost"));
    gtk_box_pack_start (GTK_BOX(frame), label, FALSE, FALSE, 0);
    gtk_widget_show(GTK_WIDGET(label));

    GtkObject *adj = gtk_adjustment_new(plvals.boost, -0.5, 7.0, 0.1, 1.0, 0.0);
    gtk_signal_connect(GTK_OBJECT(adj), "value_changed",
		       GTK_SIGNAL_FUNC(cb_boost), NULL);

    GtkWidget *scale = gtk_hscale_new(GTK_ADJUSTMENT(adj));
    gtk_range_set_update_policy(GTK_RANGE(scale), GTK_UPDATE_DISCONTINUOUS);
    gtk_scale_set_digits(GTK_SCALE(scale), 1);
    gtk_box_pack_start (GTK_BOX(frame), scale, FALSE, FALSE, 0);
    gtk_widget_show(GTK_WIDGET(scale));

    gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, FALSE, 0);
    gtk_widget_show(frame);
  }

  {
    GtkWidget *combo = (GtkWidget*)g_object_new(GIMP_TYPE_INT_COMBO_BOX, NULL);

    for (auto scene : PCDScenes)
      if (scene.first <= file_res)
	gimp_int_combo_box_append(GIMP_INT_COMBO_BOX(combo),
				  GIMP_INT_STORE_VALUE, static_cast<gint>(scene.first),
				  GIMP_INT_STORE_LABEL, scene.second.name.c_str(),
				  -1);

    gimp_int_combo_box_connect(GIMP_INT_COMBO_BOX(combo), plvals.resolution,
			       G_CALLBACK(gimp_int_combo_box_get_active),
			       &plvals.resolution);

    gtk_box_pack_start(GTK_BOX(vbox), combo, FALSE, FALSE, 0);
    gtk_widget_show(combo);
  }

  gtk_widget_show(dialog);

  gboolean run = (gimp_dialog_run(GIMP_DIALOG(dialog)) == GTK_RESPONSE_OK);

  gtk_widget_destroy (dialog);

  return run;
}

gint32 load_pcd(const gchar* filename, pcdDecode *decoder, GError*& error) {
  fs::path iceFile;
  if (plvals.resolution > static_cast<gint>(PCDResolution::Base_x16)) {
    iceFile = fs::absolute(filename);

    std::string nameOnly = iceFile.filename().native();
    size_t loc = nameOnly.find_last_of(".");
    if (loc != std::string::npos)
      nameOnly.erase(loc);
    bool useLowerCase = (nameOnly.find_last_of("img") != std::string::npos);

    iceFile = iceFile.parent_path()
      / (useLowerCase ? "ipe" : "IPE")
      / nameOnly
      / (useLowerCase ? "64base" : "64BASE")
      / (useLowerCase ? "info.ic" : "INFO.IC");
  }

  if (!decoder->readImage(iceFile, static_cast<PCDResolution>(plvals.resolution))) {
    // false here means there isn't any kind of a valid image
    g_set_error(&error, 0, 0,
		_("Decoder Error: %s\n"), decoder->getErrorString().c_str());
    delete decoder;
    return -1;
  }

  // Even if we got a valid image back, there may be warnings - let's print those:
  if (decoder->getErrorString().length() > 0) {
    fprintf (stderr, "Warning: %s\n", decoder->getErrorString().c_str());
    if (plvals.resolution > static_cast<gint>(PCDResolution::Base_x16)) {
      fprintf (stderr, " while trying to process ICE file \"%s\"\n", iceFile.c_str());
    }
  }

  decoder->setIsMonoChrome(false);
  decoder->setWhiteBalance(plvals.isD50White ? PCDWhiteBalance::D50 : PCDWhiteBalance::D65);

  decoder->postParse([](PCDResolution res, bool has_delta) {
		       if (has_delta)
			 gimp_progress_set_text_printf("Applying delta to create %s",
						       PCDScenes.at(res).name.c_str());
		     });
  decoder->setColorSpace(PCDColorSpace::LinearCCIR709);
  setup_boost(plvals.boost);

  gint32 width = decoder->getWidth(), height = decoder->getHeight();

  gint32 image_ID = gimp_image_new_with_precision(width, height,
						  GIMP_RGB, GIMP_PRECISION_FLOAT_LINEAR);
  if (image_ID == -1) {
    g_set_error(&error, 0, 0,
		_("Could not create new image for '%s': %s"),
		gimp_filename_to_utf8(filename), gimp_get_pdb_error());
    return -1;
  }

  gint32 layer_ID = gimp_layer_new(image_ID, _("Background"), width, height,
				   GIMP_RGB_IMAGE, 100, GIMP_LAYER_MODE_NORMAL_LEGACY);
  gimp_image_insert_layer(image_ID, layer_ID, -1, 0);

  const Babl *pixel_format = gimp_drawable_get_format(layer_ID);

  gimp_image_set_filename(image_ID, filename);

  GeglBuffer *buffer = gimp_drawable_get_buffer(layer_ID);
  gfloat *dest = g_new(gfloat, (gsize)width * height * 3);

  gimp_progress_set_text_printf("Converting to RGB");
  decoder->populateBuffers<float>(dest, dest + 1, dest + 2, NULL, 3,
				  [height](size_t y) {
				    if (omp_get_thread_num() == 0)
				      gimp_progress_update((gdouble)y / height);
				  });

  delete decoder;

  GeglRectangle rect = { 0, 0, width, height };
  gegl_buffer_set(buffer, &rect, 0, pixel_format, dest, GEGL_AUTO_ROWSTRIDE);
  g_free(dest);
  g_object_unref(buffer);

  return image_ID;
}

gint32 load_pcd_thumbnail(const gchar* filename, gint thumbsize, gint& width, gint& height, GimpImageType& type, GError*& error) {
  gimp_progress_init_printf(_("Opening '%s'"), gimp_filename_to_utf8(filename));

  bool isD50White = false;
  float boost = 1.0f;
  PCDResolution resolution;

  // Find an appropriate resolution to load from the file
  for (int res = static_cast<int>(PCDResolution::Base_div16); res < static_cast<int>(PCDResolution::Base_x16); res++) {
    resolution = static_cast<PCDResolution>(res);
    PCDSceneMetadata sceneMeta = PCDScenes.at(resolution);
    if ((sceneMeta.lumaWidth > thumbsize) || (sceneMeta.lumaHeight > thumbsize))
      break;
  }

  pcdDecode *decoder = new pcdDecode();
  if (decoder == nullptr) {
    g_set_error(&error, 0, 0,
		_("Could not create a decoder - probably too little memory."));
    return -1;
  }
  decoder->setInterpolation(PCDUpResMethod::LumaIterpolate);

  decoder->setIsMonoChrome(false);
  decoder->setWhiteBalance(isD50White ? PCDWhiteBalance::D50 : PCDWhiteBalance::D65);

  if (!decoder->parseHeader(filename)) {
    // false here means there isn't any kind of a valid image
    g_set_error(&error, 0, 0,
		_("Decoder Error: %s\n"), decoder->getErrorString().c_str());
    delete decoder;
    return -1;
  }

  if (!decoder->readImage("", resolution)) {
    // false here means there isn't any kind of a valid image
    g_set_error(&error, 0, 0,
		_("Decoder Error: %s\n"), decoder->getErrorString().c_str());
    delete decoder;
    return -1;
  }

  // Even if we got a valid image back, there may be warnings - let's print those:
  if (decoder->getErrorString().length() > 0) {
    fprintf (stderr, "Warning: %s\n", decoder->getErrorString().c_str());
  }

  // Set width/height to the full size that the file contains
  width = Width(decoder->getResolution(), decoder->getOrientation());
  height = Height(decoder->getResolution(), decoder->getOrientation());

  decoder->postParse([](PCDResolution res, bool has_delta) {
		       if (has_delta)
			 gimp_progress_set_text_printf("Applying delta to create %s",
						       PCDScenes.at(res).name.c_str());
		     });
  decoder->setColorSpace(PCDColorSpace::LinearCCIR709);
  setup_boost(boost);

  gint32 thumb_width = decoder->getWidth(), thumb_height = decoder->getHeight();

  gint32 image_ID = gimp_image_new_with_precision(thumb_width, thumb_height,
						  GIMP_RGB, GIMP_PRECISION_FLOAT_LINEAR);
  if (image_ID == -1) {
    g_set_error(&error, 0, 0,
		_("Could not create new image for '%s': %s"),
		gimp_filename_to_utf8(filename), gimp_get_pdb_error());
    return -1;
  }

  gint32 layer_ID = gimp_layer_new(image_ID, _("Background"), thumb_width, thumb_height,
				   GIMP_RGB_IMAGE, 100, GIMP_LAYER_MODE_NORMAL_LEGACY);
  gimp_image_insert_layer(image_ID, layer_ID, -1, 0);

  const Babl *pixel_format = gimp_drawable_get_format(layer_ID);

  gimp_image_set_filename(image_ID, filename);

  GeglBuffer *buffer = gimp_drawable_get_buffer(layer_ID);
  gfloat *dest = g_new(gfloat, (gsize)thumb_width * thumb_height * 3);

  gimp_progress_set_text_printf("Converting to RGB");
  decoder->populateBuffers<float>(dest, dest + 1, dest + 2, NULL, 3,
				  [thumb_height](size_t y) {
				    if (omp_get_thread_num() == 0)
				      gimp_progress_update((gdouble)y / thumb_height);
				  });

  delete decoder;

  GeglRectangle rect = { 0, 0, thumb_width, thumb_height };
  gegl_buffer_set(buffer, &rect, 0, pixel_format, dest, GEGL_AUTO_ROWSTRIDE);
  g_free(dest);
  g_object_unref(buffer);

  return image_ID;
}

static void query(void) {
  static const GimpParamDef load_args[] = {
    { GIMP_PDB_INT32,    "run-mode",     "The run mode { RUN-INTERACTIVE (0), RUN-NONINTERACTIVE (1) }" },
    { GIMP_PDB_STRING,   "filename",     "The name of the file to load" },
    { GIMP_PDB_STRING,   "raw-filename", "The name of the file to load" }
  };
  static const GimpParamDef load_return_vals[] = {
    { GIMP_PDB_IMAGE,   "image",         "Output image" }
  };

  gimp_install_procedure(LOAD_PROC,
			 "loads files in the Photo CD file format",
			 "loads files in the Photo CD file format",
			 "Sandy McGuffog, Ian Tester, and Contributors",
			 "Sandy McCuffog and Ian Tester",
			 "2009-2017",
			 N_("Photo CD image"),
			 NULL,
			 GIMP_PLUGIN,
			 G_N_ELEMENTS(load_args),
			 G_N_ELEMENTS(load_return_vals),
			 load_args, load_return_vals);

  gimp_register_file_handler_mime(LOAD_PROC, "image/x-photo-cd");
  gimp_register_magic_load_handler(LOAD_PROC,
				   "pcd,PCD",
				   "",
				   "2048,string,PCD_IPI");

  static const GimpParamDef thumb_args[] = {
    { GIMP_PDB_STRING, "filename",     "The name of the file to load"  },
    { GIMP_PDB_INT32,  "thumb-size",   "Preferred thumbnail size"      }
  };
  static const GimpParamDef thumb_return_vals[] = {
    { GIMP_PDB_IMAGE,  "image",        "Thumbnail image"               },
    { GIMP_PDB_INT32,  "image-width",  "Width of full-sized image"     },
    { GIMP_PDB_INT32,  "image-height", "Height of full-sized image"    }
  };

  gimp_install_procedure(LOAD_THUMB_PROC,
			 "Loads a thumbnail from a Photo CD image",
			 "Loads a thumbnail from a Photo CD image (only if it exists)",
			 "Sandy McGuffog, Ian Tester, and Contributors",
			 "Sandy McCuffog and Ian Tester",
			 "2009-2017",
			 NULL,
			 NULL,
			 GIMP_PLUGIN,
			 G_N_ELEMENTS(thumb_args),
			 G_N_ELEMENTS(thumb_return_vals),
			 thumb_args, thumb_return_vals);

  gimp_register_thumbnail_loader(LOAD_PROC, LOAD_THUMB_PROC);
}

static void run(const gchar* name,
		gint nparams, const GimpParam* param,
		gint* nreturn_vals, GimpParam** return_vals) {

  GimpRunMode run_mode = static_cast<GimpRunMode>(param[0].data.d_int32);

  G_STMT_START {
    bindtextdomain("file-pcd", gimp_locale_directory());
    bind_textdomain_codeset("file-pcd", "UTF-8");
    textdomain("file-pcd");
  } G_STMT_END;
  gegl_init(NULL, NULL);

  static GimpParam values[6];
  *nreturn_vals = 1;
  *return_vals  = values;
  values[0].type = GIMP_PDB_STATUS;
  values[0].data.d_status = GIMP_PDB_EXECUTION_ERROR;

  GError *error  = NULL;
  GimpPDBStatusType status = GIMP_PDB_SUCCESS;

  if (strcmp(name, LOAD_PROC) == 0) {
    gchar *filename = param[1].data.d_string;
    gimp_progress_init_printf(_("Opening '%s'"), gimp_filename_to_utf8(filename));

    pcdDecode *decoder = new pcdDecode();
    if (decoder == nullptr) {
      g_set_error(&error, 0, 0,
		  _("Could not create a decoder - probably too little memory."));
      values[0].data.d_status = GIMP_PDB_EXECUTION_ERROR;
      return;
    }
    decoder->setInterpolation(PCDUpResMethod::LumaIterpolate);

    if (!decoder->parseHeader(filename)) {
      // false here means there isn't any kind of a valid image
      g_set_error(&error, 0, 0,
		  _("Decoder Error: %s\n"), decoder->getErrorString().c_str());
      delete decoder;
      values[0].data.d_status = GIMP_PDB_EXECUTION_ERROR;
      return;
    }

    switch (run_mode) {
    case GIMP_RUN_INTERACTIVE:
      gimp_get_data(LOAD_PROC, &plvals);

      if (!load_dialog(decoder->getResolution()))
	status = GIMP_PDB_CANCEL;

      break;

    case GIMP_RUN_NONINTERACTIVE:
      if (nparams != 3)
	status = GIMP_PDB_CALLING_ERROR;
      break;

    case GIMP_RUN_WITH_LAST_VALS:
      gimp_get_data(LOAD_PROC, &plvals);
      break;

    default:
      break;
    }

    if (status == GIMP_PDB_SUCCESS) {
      check_load_vals();
      gint32 image_ID = load_pcd(filename, decoder, error);

      if (image_ID != -1) {
	*nreturn_vals = 2;
	values[1].type = GIMP_PDB_IMAGE;
	values[1].data.d_image = image_ID;
      } else {
	status = GIMP_PDB_EXECUTION_ERROR;
      }

      if (status == GIMP_PDB_SUCCESS)
	gimp_set_data(LOAD_PROC, &plvals, sizeof(PCDLoadVals));
    }

  } else if (strcmp(name, LOAD_THUMB_PROC) == 0) {
    if (nparams < 2) {
      status = GIMP_PDB_CALLING_ERROR;
    } else {
      gint width = 0, height = 0;
      GimpImageType type = static_cast<GimpImageType>(-1);

      gint32 image_ID = load_pcd_thumbnail(param[0].data.d_string, param[1].data.d_int32, width, height, type, error);

      if (image_ID != -1) {
	*nreturn_vals = 6;

	values[1].type = GIMP_PDB_IMAGE;
	values[1].data.d_image = image_ID;

	values[2].type = GIMP_PDB_INT32;
	values[2].data.d_int32 = width;

	values[3].type = GIMP_PDB_INT32;
	values[3].data.d_int32 = height;

	values[4].type = GIMP_PDB_INT32;
	values[4].data.d_int32 = type;

	values[5].type = GIMP_PDB_INT32;
	values[5].data.d_int32 = 1; /* num_layers */

      } else {
	status = GIMP_PDB_EXECUTION_ERROR;
      }
    }

  } else {
    status = GIMP_PDB_CALLING_ERROR;
  }

  if (status != GIMP_PDB_SUCCESS && error) {
    *nreturn_vals = 2;
    values[1].type = GIMP_PDB_STRING;
    values[1].data.d_string = error->message;
  }

  values[0].data.d_status = status;
}

const GimpPlugInInfo PLUG_IN_INFO = {
  NULL,  /* init_proc  */
  NULL,  /* quit_proc  */
  query, /* query_proc */
  run,   /* run_proc   */
};

MAIN()
