/* =======================================================
 * pcdDecode - a Photo-CD image decoder and converter
 * =======================================================
 *
 * Project Info:  http://sourceforge.net/projects/pcdtojpeg/
 * Project Lead:  Sandy McGuffog (sandy.cornerfix@gmail.com);
 *
 * (C) Copyright 2009-2011, by Sandy McGuffog and Contributors.
 *
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * ---------------
 * See pcdDecode.cc for more information
 */

#include "misc.hh"

CubicHermite boostCurve(0, 1/1.3584f, 0, 0);


//////////////////////////////////////////////////////////////
//
// Big-endian reads
//
//////////////////////////////////////////////////////////////
//

template <>
uint16_t read_be<uint16_t>(uint8_t* buffer) {
	return ((uint16_t)buffer[0] << 8)
	  | (uint16_t)buffer[1];
}

template <>
uint32_t read_be<uint32_t>(uint8_t* buffer) {
	return ((uint32_t)buffer[0] << 24)
	  | ((uint32_t)buffer[1] << 16)
	  | ((uint32_t)buffer[2] << 8)
	  | (uint32_t)buffer[3];
}

//////////////////////////////////////////////////////////////
//
// Utility String functions
//
//////////////////////////////////////////////////////////////
//

int compareBytes(const char *buffer, const char *string)
{
	size_t length = strlen(string);
	return strncmp(buffer, string, length);
}

void copyWithoutPadding(std::string& dest, const char* src, unsigned int length)
{
	for (int i = length - 1; i >= 0; i--)
		if (src[i] != ' ') {
			dest = std::string(src, i + 1);
			return;
		}
}

std::string BCD(uint8_t byte, bool pad) {
  uint8_t high = byte >> 4, low = byte & 0x0f;
  if ((high > 0) || pad)
    return std::to_string(high) + std::to_string(low);
  return std::to_string(low);
}


//////////////////////////////////////////////////////////////
//
// Huffman decoder implementation
// This is a standard table-based Huffman decoder
//
//////////////////////////////////////////////////////////////

void readHuffTable(struct hctTable& source, struct huffTable& destination, int& number)
{
	number = (source.entries)+1;
	for (long i = 0; i < 0x10000; i++) {
		destination.key[i] = 0x7f;
		destination.len[i] = kHuffmanErrorLen;
	}
#ifdef DEBUG
	fprintf(stderr, "Number of Huffman Tree entries: %d\n", *number);
#endif

	for (int i = 0; i < number; i++)
	{
		hctEntry *sub = (struct hctEntry *)(((uint8_t *)&source)+1+i*sizeof(*sub));
		unsigned int len = (unsigned int) (sub->length + 1);
		if (len > 16)
			throw "Huffman code error!!";

#ifdef DEBUG
//		fprintf(stderr, "Huffman item %d: %x len %d key %x\n", i, read_be<uint16_t>(sub->codeWord), (unsigned int) (sub->length + 1), sub->key);
#endif
		for (unsigned int index = 0; index < (0x1u << (16u-len)); index++) {
			uint16_t loc = read_be<uint16_t>(sub->codeWord) | index;
			destination.key[loc] = sub->key;
			destination.len[loc] = len;
		}
	}
}

int readNextSector(ReadBuffer& buffer)
{
	size_t n = KSectorSize;
	if (n == 0)
		return true;

	uint8_t *ptr = buffer.sbuffer;
	for(;;)
	{
		buffer.fh->read((char*)ptr, n);
		size_t d = buffer.fh->gcount();
		if (d < 1)
		  return false;

		n -= d;
		if ((n == 0) || (buffer.fh->eof() != 0))
		  break;

		ptr += d;
	}

	return true;
}

void PCDGetBits(ReadBuffer& b, int n)
{
	b.sum = (b.sum << n) & 0xffffffff;
	b.bits -= n;
	while (b.bits <= 24)
	{
		if (b.p >= (b.sbuffer+KSectorSize))
		{
			if (!readNextSector(b))
				throw "Unexpected end of file in Huffman sequence";

			b.p = b.sbuffer;
		}
		b.sum |= ((unsigned int) (*b.p) << (24 - b.bits));
		b.bits+=8;
		b.p++;
	}
}

void initReadBuffer(ReadBuffer& buffer, std::istream& file)
{
	buffer.fh = &file;
	buffer.p= buffer.sbuffer + sizeof(buffer.sbuffer);
	buffer.bits = 0;
	buffer.sum = 0;
	// Initialise the shift register
	PCDGetBits(buffer, 0);
}

void syncHuffman(ReadBuffer& b)
{
	while (!((b.sum & 0x00fff000) == 0x00fff000)) {
		PCDGetBits(b, 8);
	}
	while (!((b.sum & 0xffffff00) == 0xfffffe00)) {
		PCDGetBits(b, 1);
	}
#ifdef DEBUG
	//	fprintf(stderr, "Sync at : %d %d ftell:%d -> ", ftell(b.fp));
#endif

}

void PCDDecodeHuffman(ReadBuffer& b, struct huffTable& huf, uint8_t *dest, int length)
{
	uint8_t *ptr = dest;
	for (int i = 0; i < length; i++) {
		uint16_t code  = (b.sum >> 16) & 0xffff;
		if (huf.len[code] == kHuffmanErrorLen) {
#ifdef mInformPrintf
			fprintf(stderr, "*** Warning : Attempting to recover from Huffman sequence error......\n");
#endif
#ifdef DEBUG
			// If we're debugging, then this is almost certainly our own error
			throw "Huffman code sequence error";
#endif
			// Recovery procedure from error is to zero this sequence and just go on
			// to the next - as these are deltas, we just lose one sequence of
			// incremental information
			for (i = 0; i < length; i++)
				*dest++ = 0x0;

			syncHuffman(b);
			return;
		}

		*ptr++ = huf.key[code];
		PCDGetBits(b, huf.len[code]);
	}
}

void readAllHuffmanTables(std::istream& fh, off_t offset, huffTables* tables, int numTables)
{
	int numBytes = kSceneSectorSize * (numTables == 1 ? 1 : 2) * sizeof(uint8_t);
	uint8_t *buffer = (uint8_t *) malloc(numBytes);

	if (buffer == NULL)
		throw "memory allocation error";

	fh.seekg(offset, std::ios_base::beg);
	fh.read((char*)buffer, numBytes);

	int num = 0;
	uint8_t *ptr = buffer;
	// Read in the Huffman decoder tables, and process into something we can use
	for (int i = 0; i < numTables; i++) {
#ifdef DEBUG
		fprintf(stderr, "Processing Table number: %i\n", i);
#endif
		readHuffTable(*(struct hctTable *)ptr, tables->ht[i], num);
		// Move the pointer formward by the size of what we just read
		ptr+= sizeof(uint8_t)*(num*4 + 1);
		if ((num < 4) && (i > 0)) {
			// Assume the previous table applies(!)
			memcpy( &(tables->ht[i]), &(tables->ht[i-1]), sizeof(huffTable));
		}
	}
#ifdef DEBUG
	uint8_t eptDescriptor = *ptr++;
	fprintf(stderr, "EPT descriptor: %x\n", eptDescriptor);
#endif
	free(buffer);
}


//////////////////////////////////////////////////////////////
//
// Reader for the delta tables - this supports base, 16 base
// and 64Base
//
//////////////////////////////////////////////////////////////

bool readPCDDeltas(ReadBuffer& buf, struct huffTables* huf, PCDResolution sceneSelect, int sequenceSize, int sequencesToProcess, uint8_t *data[3], off_t colOffset)
{
	PCDSceneMetadata sceneMeta;
	try {
		sceneMeta = PCDScenes.at(sceneSelect);
	}
	catch (...) {
		return false;
	}

	if (sequencesToProcess == 0) {
		// for anything less than 64base, one sequence per row
		sequencesToProcess = (sceneSelect == PCDResolution::Base_x64) ? 1 : sceneMeta.lumaHeight + 2 * sceneMeta.chromaHeight;
	}

	int planeTrack = ((data[0] != NULL) ? 0x1 : 0) | ((data[1] != NULL) ? 0x2 : 0) | ((data[2] != NULL) ? 0x4 : 0);
	unsigned long row = 0;
	while (((planeTrack != 0x0) || (row < sceneMeta.lumaHeight)) && (sequencesToProcess > 0)) {
		// First check we're at the start of a sequence
		syncHuffman(buf);
		// Get the first 24 bits into the shift register - these have the plane, row and sequence numbers
		PCDGetBits(buf, 16);
		row = (buf.sum >> sceneMeta.rowShift) & sceneMeta.rowMask;
		unsigned int sequence = (buf.sum >> sceneMeta.sequenceShift) & sceneMeta.sequenceMask;
		unsigned long plane = (buf.sum >> sceneMeta.planeShift) & sceneMeta.planeMask;
		row *= (plane == 0 ? 1 : sceneMeta.rowSubsample);

#ifdef DEBUG
//		fprintf(stderr, "Row %d, Sequence %d, data:%x\n",  row, sequence, buf.sum);
#endif

		for (size_t count = 0; count < sceneMeta.HuffmanHeaderSize; count++) {
			// IPE headers have 32 bits of data
			PCDGetBits(buf, 8);
		}

		if (row < sceneMeta.lumaHeight) {
#ifdef DEBUG
//			fprintf(stderr, "Delta plane: %d row: %d\n", plane, row);
#endif
			switch (plane)
			{
				case 0:
				{
					PCDDecodeHuffman(buf,
							 huf->ht[0],
							 data[0] + row*sceneMeta.lumaWidth + sequence*sequenceSize + colOffset,
							 sequenceSize == 0 ? sceneMeta.lumaWidth : sequenceSize);
					planeTrack &= 0x6;
					break;
				}
				case 2:
				{
					if (data[1] != NULL) {
						PCDDecodeHuffman(buf,
								 huf->ht[1],
								 data[1]+(row>>1)*sceneMeta.chromaWidth + sequence*sequenceSize + (colOffset>>1),
								 sequenceSize == 0 ? sceneMeta.chromaWidth : sequenceSize);
					}
					planeTrack &= 0x5;
					break;
				}
				case 3:
				// Handle the strange IPE situation - plane numbers are different(!)
				case 4:
				{
					if (data[2] != NULL) {
						PCDDecodeHuffman(buf,
								 huf->ht[2],
								 data[2]+(row>>1)*sceneMeta.chromaWidth + sequence*sequenceSize + (colOffset>>1),
								 sequenceSize == 0 ? sceneMeta.chromaWidth : sequenceSize);
					}
					planeTrack &= 0x3;
					break;
				}
				default:
				{
					throw "Corrupt Image";
				}
			}
		}
		else {
#ifdef DEBUG
			fprintf(stderr, "Delta plane invalid row: %ld row: %ld\n", plane, row);
#endif
		}
		sequencesToProcess--;
	}
	return true;
}


//////////////////////////////////////////////////////////////
//
// basic "Kodak standard" bilinear upres interpolator
//
//////////////////////////////////////////////////////////////
void upResInterpolate(uint8_t *base, uint8_t *dest, uint8_t *luma, unsigned int width, unsigned int height, bool hasDeltas)
{
	unsigned int halfwidth = width >> 1, halfheight = height >> 1;

	// This is as intended by Kodak - linear interpolation
#pragma omp parallel for schedule(dynamic, 1)
	for (unsigned int row = 0; row < halfheight; row++) {
		unsigned int rowNext = row < halfheight - 1 ? halfwidth : 0;
		ptrdiff_t inputIndex = row * halfwidth, destIndex = row * 2 * width;

		uint8_t *basePix = base + inputIndex;
		float floatRow[width], floatRowDown[width];

		// First read the base values into our floating-point rows, doing simple upscaling
		float *floatPixel = floatRow, *floatPixelDown = floatRowDown;
		for (unsigned int column = 0; column < halfwidth; column++) {
			unsigned int columnNext = column < halfwidth - 1 ? 1 : 0;

			// base Pixel
			floatPixel[0] = basePix[0];

			// 01 Pixel
			floatPixel[1] = (basePix[0] + basePix[columnNext] + 1) * 0.5;

			// 10 Pixel
			floatPixelDown[0] = (basePix[0] + basePix[rowNext] + 1) * 0.5;

			// 11 Pixel
#ifdef UseFourPixels
			floatPixelDown[1] = (basePix[0] + basePix[columnNext] + basePix[rowNext] + basePix[columnNext + rowNext] + 2) * 0.25;
#else
			floatPixelDown[1] = (basePix[0] + basePix[columnNext + rowNext] + 1) * 0.5;
#endif

			basePix++;
			floatPixel += 2;
			floatPixelDown += 2;
		}

		// Add in deltas if we have them
		if (hasDeltas) {
			basePix = base + inputIndex;
			int8_t *deltaPix = (int8_t*)dest + destIndex;

			floatPixel = floatRow;
			floatPixelDown = floatRowDown;
			for (unsigned int column = 0; column < halfwidth; column++) {
				// base Pixel
				floatPixel[0] += deltaPix[0];

				// 01 Pixel
				floatPixel[1] += deltaPix[1];

				// 10 Pixel
				floatPixelDown[0] += deltaPix[width];

				// 11 Pixel
				floatPixelDown[1] += deltaPix[width + 1];

				deltaPix += 2;
				floatPixel += 2;
				floatPixelDown += 2;
			}
		}

		// Now write the rows back out to the destination buffer
		uint8_t *destPix = dest + destIndex;
		floatPixel = floatRow;
		floatPixelDown = floatRowDown;
		for (unsigned int column = 0; column < halfwidth; column++) {
			// base Pixel
			destPix[0] = floatPixel[0] < 0 ? 0
			  : floatPixel[0] > 255 ? 255
			  : (uint8_t)round(floatPixel[0]);

			// 01 Pixel
			destPix[1] = floatPixel[1] < 0 ? 0
			  : floatPixel[1] > 255 ? 255
			  : (uint8_t)round(floatPixel[1]);

			// 10 Pixel
			destPix[width] = floatPixelDown[0] < 0 ? 0
			  : floatPixelDown[0] > 255 ? 255
			  : (uint8_t)round(floatPixelDown[0]);

			// 11 Pixel
			destPix[width + 1] = floatPixelDown[1] < 0 ? 0
			  : floatPixelDown[1] > 255 ? 255
			  : (uint8_t)round(floatPixelDown[1]);

			floatPixel += 2;
			floatPixelDown += 2;
			destPix += 2;
		}
	}
}


void upResBuffer(uint8_t *base, uint8_t *dest, uint8_t *luma, unsigned int width, unsigned int height, PCDUpResMethod upResMethod, bool hasDeltas)
{
#ifdef __PerformanceAnalysis
#ifdef qMacOS
	AbsoluteTime nowTime, bgnTime;
    bgnTime = UpTime();
#endif
#endif

	if (dest != NULL) {
		if (upResMethod >= PCDUpResMethod::Iterpolate) {
			upResInterpolate(base, dest, luma, width, height, hasDeltas);

#ifdef __PerformanceAnalysis
#ifdef qMacOS
			nowTime = UpTime();
			float uSec  = HowLong(nowTime, bgnTime);
			fprintf(stderr, " upResInterpolate: %.3f usec \n", uSec);
#endif
#endif
		}
		else {
			// Here we do a very simple minded nearest neighbour look up;
			// Shouldn't be used for any serious purpose.
			unsigned int halfwidth = width >> 1;
#pragma omp parallel for schedule(dynamic, 1)
			for (unsigned int row = 0; row < height; row++) {
				// When upresing, the factor is always two
				uint8_t *lineBase = base + ((row >> 1) * halfwidth);
				int8_t *lineDelta = (int8_t*)dest + (row * width);
				for (unsigned int column = 0; column < width; column++) {
					int sum = *lineBase;
					if (hasDeltas) {
						sum += *lineDelta;
						if (sum < 0)
							sum = 0;
						else if (sum > 255)
							sum = 255;
					}

					*lineDelta = sum;

					if ((column >> 1) == 1)
						lineBase++;
					lineDelta++;
				}
			}
		}
		// Now the new base is in the old dest....
	}

}

//////////////////////////////////////////////////////////////
//
// Test code
//
//////////////////////////////////////////////////////////////

#ifdef DEBUG
void dump8by8(uint8_t *b, int width)
{
	fprintf(stderr, "\nDump8x8x\n");
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			uint8_t val = *(b + j + (i * width));
			fprintf(stderr, " %x", val);
		}
		fprintf(stderr, "\n");
	}
}

void dumpColumn(uint8_t *b, int col, int height, int width)
{
	fprintf(stderr, "\nDumpColumn\n");
	for (int i = 0; i < height; i++) {
		uint8_t val = *(b + col + (i * width));
		fprintf(stderr, " %x\n", val);
	}
}

void genTestBaseImage(PCDResolution scene, uint8_t *luma, uint8_t *chroma1, uint8_t *chroma2)
{
	PCDSceneMetadata sceneMeta;
	try {
		sceneMeta = PCDScenes.at(scene);
	}
	catch (..) {
		return;
	}

	for (int row = 0; row < sceneMeta.lumaHeight; row++) {
		for (int column = 0; column < sceneMeta.lumaWidth; column++) {
			bool block = ((row & 0x3) < 2) && ((column & 0x3) < 2);
			if (block) {
				*luma++ = 0xff;
			}
			else {
				*luma++ = 0x3f;
			}
			if (((row & 0x1) == 0x0) && ((column & 0x1) == 0x0)) {
				// write chroma
				// 156 and 137 are the "zero" values
				if (block) {
					*chroma1++ = 230;
					*chroma2++ = 230;
				}
				else {
					*chroma1++ = 156;
					*chroma2++ = 137;
				}
			}
		}
	}
}

#endif


//////////////////////////////////////////////////////////////
//
// The Micro CMM
//
//////////////////////////////////////////////////////////////

void toLinear(v4f* row, size_t columns) {
  // This is the closest I could get to the toLinearLight table
  static InterpolatedTable<float, 1024> toLinearTable([](float val) {
							if (val < 0.063)
							  return val / (4.5 * 1.3584);
							return pow((val + 0.07) / 1.07, 2.215);
						      }, 0, 1);

  v4f *pix = row;
  for (size_t col = 0; col < columns; col++, pix++)
    *pix = toLinearTable(*pix);
}

void tosRGB(v4f* row, size_t columns) {
  static InterpolatedTable<float, 1024> tosRGBTable([](float val) {
						      if (val > 0.0031308f)
							return (1.055 * pow(val, 1/2.4)) - 0.055;
						      return 12.92 * val;
						    }, 0, 1);

  v4f *pix = row;
  for (size_t col = 0; col < columns; col++, pix++)
    *pix = tosRGBTable(*pix);
}

template <>
void output_to<uint8_t>(v4f* row, size_t columns, uint8_t *red, uint8_t *green, uint8_t *blue, uint8_t *alpha, ptrdiff_t destInc) {
  v4f *pix = row;
  for (size_t col = 0; col < columns; col++, pix++) {
    *pix *= 255;
    v4si whole = { (int32_t)floor((*pix)[0]), (int32_t)floor((*pix)[1]), (int32_t)floor((*pix)[2]) };
    whole = whole < 0 ? 0
      : whole > 255 ? 255
      : whole;

    /*
    static std::random_device randdev;
    static std::mt19937 randgen(randdev());
    static std::uniform_real_distribution<float> randFrac(0, 1);
    float frac = val - whole;
    if ((randFrac(randgen) < frac) && (whole < 255))
      whole++;
    */

    *red = whole[0];
    red += destInc;

    *green = whole[1];
    green += destInc;

    *blue = whole[2];
    blue += destInc;

    if (alpha != nullptr) {
      *alpha = 255;
      alpha += destInc;
    }
  }
}

template <>
void output_to<uint16_t>(v4f* row, size_t columns, uint16_t *red, uint16_t *green, uint16_t *blue, uint16_t *alpha, ptrdiff_t destInc) {
  v4f *pix = row;
  for (size_t col = 0; col < columns; col++, pix++) {
    *pix *= 65535;
    v4si whole = { (int32_t)floor((*pix)[0]), (int32_t)floor((*pix)[1]), (int32_t)floor((*pix)[2]) };
    whole = whole < 0 ? 0
      : whole > 65535 ? 65535
      : whole;

    /*
    static std::random_device randdev;
    static std::mt19937 randgen(randdev());
    static std::uniform_real_distribution<float> randFrac(0, 1);
    float frac = val - whole;
    if ((randFrac(randgen) < frac) && (whole < 65535))
      whole++;
    */

    *red = whole[0];
    red += destInc;

    *green = whole[1];
    green += destInc;

    *blue = whole[2];
    blue += destInc;

    if (alpha != nullptr) {
      *alpha = 65535;
      alpha += destInc;
    }
  }
}

template <>
void output_to<float>(v4f* row, size_t columns, float *red, float *green, float *blue, float *alpha, ptrdiff_t destInc) {
  v4f *pix = row;
  for (size_t col = 0; col < columns; col++, pix++) {
    *pix = *pix < 0 ? 0
      : *pix > 65535 ? 65535
      : *pix;

    *red = (*pix)[0];
    red += destInc;

    *green = (*pix)[1];
    green += destInc;

    *blue = (*pix)[2];
    blue += destInc;

    if (alpha != nullptr) {
      *alpha = 1;
      alpha += destInc;
    }
  }
}


//////////////////////////////////////////////////////////////
//
// Base (and lower) image reader
//
//////////////////////////////////////////////////////////////


PCDResolution readBaseImage(std::istream& fh, PCDResolution scene, int ICDOffset[MaxPCDScenes], uint8_t*& luma, uint8_t*& chroma1, uint8_t*& chroma2)
{
	// Base image scene number......
	if (scene > PCDResolution::Base)
		scene = PCDResolution::Base;
	bool haveReadBase = false;

	int sceneNumber = static_cast<int>(scene);
	while (!haveReadBase && (sceneNumber >= static_cast<int>(PCDResolution::Base_div16))) {
		try {
			PCDSceneMetadata sceneMeta = PCDScenes.at(scene);
			size_t numBytes = sceneMeta.lumaWidth * sceneMeta.lumaHeight * sizeof(uint8_t) + 1;
			luma = (uint8_t *) malloc(numBytes);
			chroma1 = (uint8_t *) malloc(numBytes >> 2);
			chroma2 = (uint8_t *) malloc(numBytes >> 2);

			if ((luma == NULL) || (chroma1 == NULL) || (chroma2 ==  NULL))
				throw "Memory allocation error";

			// Read interleaved image.
			fh.seekg(kSceneSectorSize * ICDOffset[sceneNumber], std::ios_base::beg);
			long y;
			size_t count = 0;
			for (y=0; y < (long) (sceneMeta.chromaHeight); y++)
			{
				fh.read((char*)luma + y * 2 * sceneMeta.lumaWidth, sceneMeta.lumaWidth);
				count += fh.gcount();

				fh.read((char*)luma + (y * 2 + 1) * sceneMeta.lumaWidth, sceneMeta.lumaWidth);
				count += fh.gcount();

				fh.read((char*)chroma1 + y * sceneMeta.chromaWidth, sceneMeta.chromaWidth);
				count += fh.gcount();

				fh.read((char*)chroma2 + y * sceneMeta.chromaWidth, sceneMeta.chromaWidth);
				count += fh.gcount();
			}
			if (count != ((sceneMeta.lumaWidth * 2 + sceneMeta.chromaWidth * 2) * sceneMeta.chromaHeight))
				throw "File ended unexpectedly";

			haveReadBase = true;
		}
		catch (...) {
			if (luma != NULL) {
				free(luma);
				luma = NULL;
			}
			if (chroma1 != NULL) {
				free(chroma1);
				chroma1 = NULL;
			}
			if (chroma2 != NULL) {
				free(chroma2);
				chroma2 = NULL;
			}
			sceneNumber--;
			scene = static_cast<PCDResolution>(sceneNumber);
		}
	}
	return scene;
}

size_t Width(PCDResolution res, PCDOrientation rot) {
  try {
    PCDSceneMetadata resMeta = PCDScenes.at(res);
    switch (rot) {
    case PCDOrientation::Rot90:
    case PCDOrientation::Rot270:
      return resMeta.lumaHeight;

    case PCDOrientation::Rot0:
    case PCDOrientation::Rot180:
    default:
      return resMeta.lumaWidth;
    }
  }
  catch (...) {
  }
  return 0;
}

size_t Height(PCDResolution res, PCDOrientation rot) {
  try {
    PCDSceneMetadata resMeta = PCDScenes.at(res);
    switch (rot) {
    case PCDOrientation::Rot90:
    case PCDOrientation::Rot270:
      return resMeta.lumaWidth;

    case PCDOrientation::Rot0:
    case PCDOrientation::Rot180:
    default:
      return resMeta.lumaHeight;
    }
  }
  catch (...) {
  }
  return 0;
}
