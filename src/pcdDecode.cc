/* =======================================================
 * pcdDecode - a Photo-CD image decoder and converter
 * =======================================================
 *
 * Project Info:  http://sourceforge.net/projects/pcdtojpeg/
 * Project Lead:  Sandy McGuffog (sandy.cornerfix@gmail.com);
 *
 * (C) Copyright 2009-2011, by Sandy McGuffog and Contributors.
 *
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * ---------------
 * pcdDecode is written to provide a PCD decoder library that is:
 * 1. Easy to use, with a clear API, and human readable messages
 * 2. Suited to modern multi-core processors; so multithreaded where applicable
 * 3. Multi-platform - OS X, Windows, *nix, little- or big-endian
 * 4. Minimum dependencies - pcdDecode will use OpenMP if available, but is
 *    otherwise standalone (other than for normal C++ libraries)
 * 5. Correctly converts all forms of PCD files, including all classes of huffman
 *    encoding (classes 1-4)
 * 6. Converts all PCD resolutions, including 64Base images
 * 7. Correct and predictable color space handling (no blown highlights, no color casts);
 *    RGB data can be returned in a choice of well specified color spaces.
 * 8. Full support for "Kodak standard" bilinear interpolation of chroma
 * 9. Extracts PCD file metadata information
 * 10. Transparently recovers from many data errors
 * 11. GPL licensed
 *
 * pcdDecode is also available is non-GPL version that adds AHD style interpolation;
 * this offers lower noise and better edge performance.
 *
 * ---------------
 * Compiling: pcdDecode should compile without modification on most systems. It has been
 * tested on OS X (Xcode), MS Windows (Visual C++) and linux (GCC), on both big- and
 * little-endian CPUs
 *
 * ---------------
 * pcdDecode.cpp
 * ---------------
 * (C) Copyright 2009-2011, by Sandy McGuffog and Contributors.
 *
 * Original Author:  Sandy McGuffog;
 * Contributor(s):   -;
 *
 * Acknowlegements:   Hadmut Danisch (danisch@ira.uka.de), who authored hpcdtoppm in the early
 *                    90's. Although this software itself is unrelated to hpcdtoppm, the pcdDecoder
 *                    package would not have been possible without Hadmut's original reverse
 *                    engineering of the PCD file format.
 *
 *                    Ted Felix (http://tedfelix.com/ ), who provided sample PCD images and assorted
 *                    PCD documentation, as well as testing early versions of this software
 *
 * Changes
 * -------
 *
 * V1.0.2 - 1 July 2009 - Added YCC color space to the decoder library
 *                        Improved sRGB shadow detail and color handling
 *
 * V1.0.3 - 1 Sept 2009 - More descriptive error messages
 *
 * V1.0.4 - 19 Sept 2009 - Multithreading under Windows
 *
 * V1.0.5 - 21 Sept 2009 - More efficient memory management
 *
 * V1.0.6 - 12 Jan 2010 - Fix for compile problems under Ubuntu
 *
 * V1.0.7 - 12 Feb 2010 - Fix bug in advanced iterpolation routine
 *                        Note this has no impact on the GPL version
 *
 * V1.0.8 - 20 Mar 2010 - Fix bug in the conversion of 64base images that
 *                        have a non-native aspect ratio
 *
 * V1.0.9 - 23 Sept 2010 - Fix bug in the conversion of 64base images that
 *                        have long sequence lengths; modifications to allow
 *                        easy compilation as a DLL
 *
 * V1.0.10 - 28 March 2011 - Work around for a bug in GCC that incorrectly handles
 *                           nested structures on the stack
 *
 * V1.0.12 - 28 April 2015 - Cosmetic changes to avoid warnings when using later
 *                           version of Xcode
 */

#include "pcdDecode.hh"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>

//////////////////////////////////////////////////////////////
//
// Class initialiser and destructors
//
//////////////////////////////////////////////////////////////
pcdDecode::pcdDecode(void)
{
	luma = NULL;
	chroma1 = NULL;
	chroma2 = NULL;
	int i, j;
	for (i = 0; i < 3; i++) {
		for (j = 0; j < 3; j++) {
			deltas[i][j] = NULL;
		}
	}
	upResMethod = PCDUpResMethod::LumaIterpolate;
	pcdFile = NULL;
	colorSpace = PCDColorSpace::Raw;			// Default for PCD
	whiteBalance = PCDWhiteBalance::D65;			// Default for PCD
	monochrome = false;
	// Next line only used if we aren't using static LUTs
//	 populateLUTs();
}

pcdDecode::~pcdDecode()
{
	pcdFreeAll();
}

void pcdDecode::pcdFreeAll(void)
{
	if (luma != NULL) free(luma);
	luma = NULL;
	if (chroma1 != NULL) free(chroma1);
	chroma1 = NULL;
	if (chroma2 != NULL) free(chroma2);
	chroma2 = NULL;
	if (pcdFile != NULL) free(pcdFile);
	pcdFile = NULL;
	int i, j;
	for (i = 0; i < 3; i++) {
		for (j = 0; j < 3; j++) {
			if (deltas[i][j] != NULL) free(deltas[i][j]);
			deltas[i][j] = NULL;
		}
	}
}

//////////////////////////////////////////////////////////////
//
// Class accessors
//
//////////////////////////////////////////////////////////////

size_t pcdDecode::getWidth()
{
	return Width(scene, imageRotate);
}

size_t pcdDecode::getHeight()
{
	return Height(scene, imageRotate);
}

void pcdDecode::setInterpolation(PCDUpResMethod value)
{
	upResMethod = value;
}

void pcdDecode::setColorSpace(PCDColorSpace value)
{
	colorSpace = value;
}

PCDColorSpace pcdDecode::getColorSpace()
{
	return colorSpace;
}

void pcdDecode::setWhiteBalance(PCDWhiteBalance value)
{
	whiteBalance = value;
}

std::string pcdDecode::getErrorString()
{
	return errorString;
}

bool pcdDecode::isMonochrome()
{
	return monochrome;
}

void pcdDecode::setIsMonoChrome(bool val) {
	monochrome = monochrome | val;
}

long pcdDecode::digitisationTime() {
	if (pcdFile != NULL)
		return read_be<uint32_t>(pcdFile->ipiHeader.imageScanningTime);

	return 0;
}

void pcdDecode::getFilmTermData(int& FTN, int& PC, int& GC) {
	if ((pcdFile != NULL) && (compareBytes(pcdFile->ipiHeader.sbaSignature,"SBA")) == 0) {
		int ftn = read_be<uint16_t>(pcdFile->ipiHeader.sbaFTN);
		auto result = PCDFTN_PC_GC_Medium.find(ftn);
		if (result != PCDFTN_PC_GC_Medium.end()) {
			FTN = result->first;
			PC = result->second.PC;
			GC = result->second.GC;
			return;
		}
	}

	FTN = 0;
	PC = 0;
	GC = 0;
}

void pcdDecode::getMetadata(PCDMetaDataDictionary select, std::string& description, std::string& value)
{
	if (pcdFile == NULL) {
		description = "Error";
		value = "Error";
		return;
	}

	description = PCDMetadataDescriptions.at(select);

	if (compareBytes(pcdFile->ipiHeader.ipiSignature,"PCD_IPI") == 0) {
		time_t t;
		struct tm *brokentime;
		char *temp;
		switch (select) {
			case PCDMetaDataDictionary::specificationVersion:
				if (read_be<uint32_t>(pcdFile->ipiHeader.specificationVersion) == 0xffff) {
					value = "-";
				}
				else {
					value = std::to_string(pcdFile->ipiHeader.specificationVersion[0]) + "." + std::to_string(pcdFile->ipiHeader.specificationVersion[1]);
				}
				break;
			case PCDMetaDataDictionary::authoringSoftwareRelease:
				if (read_be<uint32_t>(pcdFile->ipiHeader.authoringSoftwareRelease) == 0xffff) {
					value = "-";
				}
				else {
					value = std::to_string(pcdFile->ipiHeader.authoringSoftwareRelease[0]) + "." + std::to_string(pcdFile->ipiHeader.authoringSoftwareRelease[1]);
				}
				break;
			case PCDMetaDataDictionary::imageMagnificationDescriptor:
				value = "×" + BCD(pcdFile->ipiHeader.imageMagnificationDescriptor[0], false) + "." + BCD(pcdFile->ipiHeader.imageMagnificationDescriptor[1]);
				break;
			case PCDMetaDataDictionary::imageScanningTime:
				// Note we make the glorious assumption that the C library is Posix compliant in that
				// time_t is seconds since 1/1/1970.
				if (read_be<uint32_t>(pcdFile->ipiHeader.imageScanningTime) == 0xffff) {
				  value = "-";
				}
				else {
					t = read_be<uint32_t>(pcdFile->ipiHeader.imageScanningTime);
					brokentime = localtime(&t);
					temp = asctime(brokentime);
					// Get rid of the newline
					temp[strlen(temp)-1] = 0x0;
					value = temp;
				}
				break;
			case PCDMetaDataDictionary::imageModificationTime:
				// Note we make the glorious assumption that the C library is Posix compliant in that
				// time_t is seconds since 1/1/1970
				if (read_be<uint32_t>(pcdFile->ipiHeader.imageModificationTime) == 0xffff) {
				  value = "-";
				}
				else {
					t = read_be<uint32_t>(pcdFile->ipiHeader.imageModificationTime);
					brokentime = localtime(&t);
					temp = asctime (brokentime);
					// Get rid of the newline
					temp[strlen(temp)-1] = 0x0;
					value = temp;
				}
				break;
			case PCDMetaDataDictionary::imageMedium:
				if (pcdFile->ipiHeader.imageMedium < PCDMediumTypes.size()) {
					value = PCDMediumTypes[pcdFile->ipiHeader.imageMedium];
				}
				else {
					value = "-";
				}
				break;
			case PCDMetaDataDictionary::productType:
				copyWithoutPadding(value, pcdFile->ipiHeader.productType, sizeof(pcdFile->ipiHeader.productType));
				break;
			case PCDMetaDataDictionary::scannerVendorIdentity:
				copyWithoutPadding(value, pcdFile->ipiHeader.scannerVendorIdentity, sizeof(pcdFile->ipiHeader.scannerVendorIdentity));
				break;
			case PCDMetaDataDictionary::scannerProductIdentity:
				copyWithoutPadding(value, pcdFile->ipiHeader.scannerProductIdentity, sizeof(pcdFile->ipiHeader.scannerProductIdentity));
				break;
			case PCDMetaDataDictionary::scannerFirmwareRevision:
				copyWithoutPadding(value, pcdFile->ipiHeader.scannerFirmwareRevision, sizeof(pcdFile->ipiHeader.scannerFirmwareRevision));
				break;
			case PCDMetaDataDictionary::scannerFirmwareDate:
				copyWithoutPadding(value, pcdFile->ipiHeader.scannerFirmwareDate, sizeof(pcdFile->ipiHeader.scannerFirmwareDate));
				break;
			case PCDMetaDataDictionary::scannerSerialNumber:
				copyWithoutPadding(value, pcdFile->ipiHeader.scannerSerialNumber, sizeof(pcdFile->ipiHeader.scannerSerialNumber));
				break;
			case PCDMetaDataDictionary::scannerPixelSize:
				// BCD(!) coded
				value = BCD(pcdFile->ipiHeader.scannerPixelSize[0], false) + "." + BCD(pcdFile->ipiHeader.scannerPixelSize[1]);
				break;
			case PCDMetaDataDictionary::piwEquipmentManufacturer:
				copyWithoutPadding(value, pcdFile->ipiHeader.piwEquipmentManufacturer, sizeof(pcdFile->ipiHeader.piwEquipmentManufacturer));
				break;
			case PCDMetaDataDictionary::photoFinisherName:
				// Don't return anything with a really exotic character set; the chances that
				// it will be displayed correctly are negligable.
				if (pcdFile->ipiHeader.photoFinisherCharSet < 5) {
					copyWithoutPadding(value, pcdFile->ipiHeader.photoFinisherName, sizeof(pcdFile->ipiHeader.photoFinisherName));
				}
				else {
					value = "-";
				}
				break;
			case PCDMetaDataDictionary::sbaRevision:
				if ((compareBytes(pcdFile->ipiHeader.sbaSignature,"SBA") != 0) || (read_be<uint32_t>(pcdFile->ipiHeader.specificationVersion) == 0xffff)) {
					value = "-";
				}
				else {
					value = std::to_string(pcdFile->ipiHeader.specificationVersion[0]) + "." + std::to_string(pcdFile->ipiHeader.specificationVersion[1]);
				}
				break;
			case PCDMetaDataDictionary::sbaCommand:
				if ((compareBytes(pcdFile->ipiHeader.sbaSignature,"SBA") != 0) || (pcdFile->ipiHeader.sbaCommand >= PCDSBATypes.size())) {
					value = "-";
				}
				else {
					value = PCDSBATypes[pcdFile->ipiHeader.sbaCommand];
				}
				break;
			case PCDMetaDataDictionary::sbaFilm:
				if (compareBytes(pcdFile->ipiHeader.sbaSignature,"SBA") != 0) {
					value = "-";
				}
				else {
					int ftn = read_be<uint16_t>(pcdFile->ipiHeader.sbaFTN);
					auto result = PCDFTN_PC_GC_Medium.find(ftn);
					if (result == PCDFTN_PC_GC_Medium.end()) {
						value = "Unknown film";
					}
					else {
						value = result->second.MediumName;
					}
				}
				break;
			case PCDMetaDataDictionary::copyrightStatus:
				if (pcdFile->ipiHeader.copyrightStatus == 0x1) {
					value = "Copyright restrictions apply - see copyright file on original CD-ROM for details";
				}
				else {
					value = "Copyright restrictions not specified";
				}
				break;
			case PCDMetaDataDictionary::copyrightFile:
				if (pcdFile->ipiHeader.copyrightStatus == 0x1) {
					copyWithoutPadding(value, pcdFile->ipiHeader.copyrightFile, sizeof(pcdFile->ipiHeader.copyrightFile));
				}
				else {
					value = "-";
				}
				break;
			case PCDMetaDataDictionary::compressionClass:
				value = PCDHuffmanClasses[imageHuffmanClass];
				break;
			default:
				value = "-";
				break;
		}
	}
	else {
		value = "-";
	}
}

void pcdDecode::interpolateBuffers(uint8_t **c1UpRes, uint8_t **c2UpRes, int *resFactor)
{
	// This does an interpolate either by a factor of 2 or 4
	uint8_t *lp, *c1p, *c2p, *intermediate;
	lp = luma;
	c1p = chroma1;
	c2p = chroma2;
	intermediate = NULL;
	PCDSceneMetadata sceneMeta;
	try {
		sceneMeta = PCDScenes.at(scene);
	}
	catch (...) {
		return;
	}

#ifdef DEBUG
	//	dumpColumn(lp, 356, sceneMeta.lumaHeight, sceneMeta.lumaWidth);
	//	dump8by8(c1p, sceneMeta.chromaWidth);
#endif

	if (upResMethod >= PCDUpResMethod::Iterpolate) {
		// Linear interpolation..........
		*c1UpRes = (uint8_t *) malloc(sceneMeta.lumaHeight*sceneMeta.lumaWidth*sizeof(uint8_t));
		*c2UpRes = (uint8_t *) malloc(sceneMeta.lumaHeight*sceneMeta.lumaWidth*sizeof(uint8_t));
		if (*c1UpRes == NULL || *c1UpRes == NULL)
			throw "Memory Error!";

		if (*resFactor == 2) {
			intermediate = (uint8_t *) malloc((sceneMeta.lumaHeight>>1)*(sceneMeta.lumaWidth>>1)*sizeof(uint8_t));
			if (intermediate == NULL)
				throw "Memory Error!";

			upResBuffer(c1p, intermediate, NULL, sceneMeta.lumaWidth>>1, sceneMeta.lumaHeight>>1, upResMethod, false);
			c1p = intermediate;
#ifdef DEBUG
			dump8by8(c1p, sceneMeta.lumaWidth>>1);
#endif
		}

		upResBuffer(c1p, *c1UpRes, lp, sceneMeta.lumaWidth, sceneMeta.lumaHeight, upResMethod, false);
		c1p = *c1UpRes;

		if (*resFactor == 2) {
			upResBuffer(c2p, intermediate, NULL, sceneMeta.lumaWidth>>1, sceneMeta.lumaHeight>>1, upResMethod, false);
			c2p = intermediate;
		}
		upResBuffer(c2p, *c2UpRes, lp, sceneMeta.lumaWidth, sceneMeta.lumaHeight, upResMethod, false);
		c2p = *c2UpRes;


		if (intermediate != NULL) {
			free(intermediate);
			intermediate = NULL;
		}
		*resFactor = 0;
	}
}

PCDOrientation pcdDecode::getOrientation()
{
	return imageRotate;
}

PCDResolution pcdDecode::getScene(void) {
	return scene;
}

PCDResolution pcdDecode::getResolution(void) {
	return imageResolution;
}

void pcdDecode::postParse(std::function<void(PCDResolution, bool)> scene_cb)
{
	bool haveDeltas;

	if (pcdFile == NULL)
		return;

	int Base_x4 = static_cast<int>(PCDResolution::Base_x4);
	for (int sceneNumber = Base_x4; sceneNumber <= static_cast<int>(PCDResolution::Base_x64); sceneNumber++) {
		PCDSceneMetadata sceneMeta;
		try {
			sceneMeta = PCDScenes.at(static_cast<PCDResolution>(sceneNumber));
		}
		catch (...) {
			return;
		}

		uint8_t **deltas_scene = deltas[sceneNumber - Base_x4];
		// Iterate the possible deltas that are avalable......
		if (deltas_scene[0] != NULL) {
			// First the luma delta....
			upResBuffer(luma, deltas_scene[0], NULL, sceneMeta.lumaWidth, sceneMeta.lumaHeight, min(PCDUpResMethod::Iterpolate, upResMethod), true);
			if (deltas_scene[0] != NULL) {
				free(luma);
				luma = deltas_scene[0];
				deltas_scene[0] = NULL;
			}
			// If there is a luma delta, we have to upres the chromas as well.....
			haveDeltas = (deltas_scene[1] != NULL);
			if (!haveDeltas)
				deltas_scene[1] = (uint8_t *) malloc((sceneMeta.lumaWidth>>1) * (sceneMeta.lumaHeight>>1)*sizeof(uint8_t));

			upResBuffer(chroma1, deltas_scene[1], NULL, sceneMeta.lumaWidth>>1, sceneMeta.lumaHeight>>1, min(PCDUpResMethod::Iterpolate, upResMethod), haveDeltas);
			if (deltas_scene[1] != NULL) {
				free(chroma1);
				chroma1 = deltas_scene[1];
				deltas_scene[1] = NULL;
			}

			haveDeltas = (deltas_scene[2] != NULL);
			if (!haveDeltas)
				deltas_scene[2] = (uint8_t *) malloc((sceneMeta.lumaWidth>>1) * (sceneMeta.lumaHeight>>1)*sizeof(uint8_t));

			upResBuffer(chroma2, deltas_scene[2], NULL, sceneMeta.lumaWidth>>1, sceneMeta.lumaHeight>>1, min(PCDUpResMethod::Iterpolate, upResMethod), haveDeltas);
			if (deltas_scene[2] != NULL) {
				free(chroma2);
				chroma2 = deltas_scene[2];
				deltas_scene[2] = NULL;
			}

			if (scene_cb != nullptr)
				scene_cb(static_cast<PCDResolution>(sceneNumber), true);
		} else
			if (scene_cb != nullptr)
				scene_cb(static_cast<PCDResolution>(sceneNumber), false);

	}
}

//////////////////////////////////////////////////////////////
//
// Structures for the 64Base files
//
//////////////////////////////////////////////////////////////

struct ic_header {
	char ic_name[0x28];
	uint8_t val1[2];
	uint8_t val2[2];
	uint8_t off_descr[4];
	uint8_t off_fnames[4];
	uint8_t off_pointers[4];
	uint8_t off_huffman[4];
};

struct ic_description {
	uint8_t len[2];
	uint8_t color;
	uint8_t fill;
	uint8_t width[2];
	uint8_t height[2];
	uint8_t offset[2];
	uint8_t length[4];
	uint8_t off_pointers[4];
	uint8_t off_huffman[4];
	uint8_t fill2[6];
};


struct ic_fname  {
	char fname[12];
	uint8_t size[4];
};

struct ic_entry {
	uint8_t fno[2];
	uint8_t offset[4];
};

bool pcdDecode::parseICFile (const fs::path ipe_file)
{
	std::ifstream ic;
	std::ifstream thisFile;
	struct ic_header *header;
	struct ic_description *description[3];
	struct ic_fname *names[10];
	bool retVal = true;
	fs::path processedFNames[10];

	ReadBuffer hufBuffer;

	int Base_x4 = static_cast<int>(PCDResolution::Base_x4);
	int Base_x64 = static_cast<int>(PCDResolution::Base_x64);

	uint8_t *buffer = NULL;

	if (ipe_file.native().size() < 10) {
		errorString = "IPE filename too short to be valid";
		return false;
	}
	// Check the E of 64BASE to determine whether we have a lower case environment
	bool usingLowerCase = (ipe_file.native()[ipe_file.native().size() - 9] == 'e');

	ic.open(ipe_file.native(), std::ios_base::in | std::ios_base::binary);
	if (ic.fail()) {
		errorString = "Could not open 64Base IPE file";
		return false;
	}

	// Find the total file size
	ic.seekg(0, std::ios_base::end);
	std::istream::pos_type fileSize = (ic.tellg() / KSectorSize) + 1;
	if (fileSize < 1) {
		errorString = "Could not read 64Base IPE file";
		return false;
	}

	huffTables *hTables = (huffTables *) malloc(sizeof(huffTables));
	if (hTables == NULL) {
		errorString = "Could not allocate huffman tables";
		return false;
	}

	uint8_t **deltas_x64 = deltas[Base_x64 - Base_x4];
	try {
		// Read the whole file in......
		buffer = (uint8_t *)malloc(fileSize * KSectorSize * sizeof(uint8_t));
		if (buffer == NULL)
			throw "Memory allocation error";

		ic.seekg(0, std::ios_base::beg);
		ic.read((char*)buffer, KSectorSize * fileSize);
		if (static_cast<std::istream::pos_type>(ic.gcount() + 1) < fileSize)
			throw "IC File too small";

		header = (ic_header *) buffer;
		ipeLayers = read_be<uint16_t>(buffer + read_be<uint32_t>(header->off_descr));

		if (!((ipeLayers==1) || (ipeLayers==3)))
			throw "Invalid number of layers";

		if (monochrome) {
			// Override
			ipeLayers = 1;
		}
		// Read the layer descriptions......
		description[0] = (ic_description *) (buffer + read_be<uint32_t>(header->off_descr) + sizeof(uint16_t));
		description[1] = (ic_description *) (((uint8_t *) description[0]) + read_be<uint16_t>(description[0]->len));
		description[2] = (ic_description *) (((uint8_t *) description[1]) + read_be<uint16_t>(description[1]->len));

		// Now read the filenames.....
		ipeFiles = read_be<uint16_t>(buffer + read_be<uint32_t>(header->off_fnames));

		if ((ipeFiles<1) || (ipeFiles>10) || (ipeFiles < ipeLayers))
			throw "Invalid number of IPE files";

		for (int i = 0; i < ipeFiles; i++) {
			names[i] = (ic_fname *) (buffer + read_be<uint32_t>(header->off_fnames) + sizeof(ic_fname)*i + sizeof(uint16_t));
			fs::path::string_type temp_name = names[i]->fname;
			if (usingLowerCase) {
				for (unsigned int j = 0; j < processedFNames[i].native().size(); j++) {
					// Using tolower here is ok; we know the encoding is straight ASCII
					temp_name[j] = tolower(temp_name[j]);
				}
			}
			processedFNames[i] = temp_name;
		}

		// Read the Huffman tables........
		readAllHuffmanTables(ic, read_be<uint32_t>(header->off_huffman), hTables, ipeLayers);

		PCDSceneMetadata Base_x64_meta = PCDScenes.at(PCDResolution::Base_x64);
		deltas_x64[0] = (uint8_t *) malloc(Base_x64_meta.lumaWidth * Base_x64_meta.lumaHeight * sizeof(uint8_t));
		memset(deltas_x64[0], 0x0, Base_x64_meta.lumaWidth * Base_x64_meta.lumaHeight * sizeof(uint8_t));
		if (ipeLayers == 3) {
			deltas_x64[1] = (uint8_t *) malloc(Base_x64_meta.chromaWidth * Base_x64_meta.chromaHeight * sizeof(uint8_t));
			deltas_x64[2] = (uint8_t *) malloc(Base_x64_meta.chromaWidth * Base_x64_meta.chromaHeight * sizeof(uint8_t));
			memset(deltas_x64[1], 0x0, Base_x64_meta.chromaWidth * Base_x64_meta.chromaHeight * sizeof(uint8_t));
			memset(deltas_x64[2], 0x0, Base_x64_meta.chromaWidth * Base_x64_meta.chromaHeight * sizeof(uint8_t));
		}

		int currentFile = 0;
		for (int layer = 0; layer< ipeLayers; layer++) {
#ifdef DEBUG
			fprintf(stderr, "len: %d\n", read_be<uint16_t>((uint8_t*) &description[layer]->len));
			fprintf(stderr, "color: %d\n", description[layer]->color);
			fprintf(stderr, "fill: %d\n", description[layer]->fill);
			fprintf(stderr, "width: %d\n", read_be<uint16_t>((uint8_t*) &description[layer]->width));
			fprintf(stderr, "height: %d\n", read_be<uint16_t>((uint8_t*) &description[layer]->height));
			fprintf(stderr, "offset: %d\n", read_be<uint16_t>((uint8_t*) &description[layer]->offset));
			fprintf(stderr, "length: %d\n", read_be<uint32_t>((uint8_t*) &description[layer]->length));
			fprintf(stderr, "off_pointers: %d\n", read_be<uint32_t>((uint8_t*) &description[layer]->off_pointers));
			fprintf(stderr, "off_huffman: %d\n", read_be<uint32_t>((uint8_t*) &description[layer]->off_huffman));
#endif
			// Iterate through how ever many sectors there are
			// we pass entire files to the Huffman decoder; all the row and sequence info comes
			// out of the information encoded in the Huffman sequence headers
			int sequenceSize = read_be<uint32_t>((uint8_t*) &description[layer]->length);
			int numSequences = read_be<uint16_t>((uint8_t*) &description[layer]->width)*read_be<uint16_t>((uint8_t*) &description[layer]->height)/sequenceSize;
			int sequence = 0;
			struct ic_entry *entry = (ic_entry *) (buffer + read_be<uint32_t>((uint8_t*) &description[layer]->off_pointers));
			currentFile = read_be<uint16_t>((uint8_t*) entry->fno);
			size_t startPoint = read_be<uint32_t>((uint8_t*) entry->offset);
			while (numSequences-- > 0) {
#ifdef DEBUG
//				fprintf(stderr, "File No %d, offset %d\n",  read_be<uint16_t>((uint8_t*) entry->fno), read_be<uint32_t>((uint8_t*) entry->offset));
#endif
				sequence++;
				if ((currentFile != read_be<uint16_t>((uint8_t*) entry->fno)) || (numSequences == 0)) {
					fs::path thisFilePath = const_cast<fs::path*>(&ipe_file)->parent_path() / processedFNames[currentFile];
					thisFile.open(thisFilePath.native(), std::ios_base::in | std::ios_base::binary);
					if (thisFile.fail())
						throw "Could not open 64Base extension image";

					thisFile.seekg((long) startPoint, std::ios_base::beg);
					initReadBuffer(hufBuffer, thisFile);
					readPCDDeltas(hufBuffer, hTables, PCDResolution::Base_x64, sequenceSize, sequence-1, deltas_x64, read_be<uint16_t>((uint8_t*) &description[layer]->offset));
#ifdef DEBUG
					uint8_t *test = deltas_x64[1];
					test += ((Base_x64_meta.chromaWidth * Base_x64_meta.chromaHeight * sizeof(uint8_t)) >> 1) -32 -224;
#endif
					thisFile.close();
					currentFile = read_be<uint16_t>((uint8_t*) entry->fno);
					startPoint = read_be<uint32_t>((uint8_t*) entry->offset);
					sequence = 0;
				}
				entry++;
			}
		}
	}
	catch (char *err) {
		if (errorString.length() == 0)
			errorString = std::string(err) + " while processing 64Base image";
		retVal = false;
	}
	catch (...) {
		if (errorString.length() == 0)
			errorString = "Error while processing 64Base image";
		retVal = false;
	}
	if (!retVal) {
		int i;
		for(i = 0; i < 3; i++ ) {
			if (deltas_x64[i] != NULL) {
				free(deltas_x64[i]);
				deltas_x64[i] = NULL;
			}
		}
	}

	if (hTables != NULL) {
		free(hTables);
	}

	if (ic.is_open())
		ic.close();
	if (thisFile.is_open())
		thisFile.close();
	if (buffer != NULL) {
		free(buffer);
		buffer = NULL;
	}

	return retVal;
}


bool pcdDecode::parseHeader (const fs::path in_file)
{
	size_t count = 0;
	bool overview;

	int Base_x16 = static_cast<int>(PCDResolution::Base_x16);

	// Free any memory from previous conversions
	pcdFreeAll();
	errorString.clear();

	// Close the file handle just in case
	if (fh.is_open())
		fh.close();

	fh.open(in_file.native(), std::ios_base::in | std::ios_base::binary);
	if (fh.fail())
	{
		errorString = "Could not open PCD file - may be a file permissions problem";
		return false;
	}

	// Check that this is a PCD file.
	pcdFile = (PCDFile*)malloc(sizeof(PCDFile));
	if (pcdFile == NULL)
		return false;

	fh.read((char*)pcdFile, sizeof(PCDFile));
	count = fh.gcount();
	if (count != sizeof(PCDFile)) {
		free(pcdFile);
		pcdFile = NULL;
		errorString = "PCD file is too small to be valid";
		return false;
	}
	overview = compareBytes(pcdFile->header.signature,"PCD_OPA") == 0;

	if ((compareBytes(pcdFile->ipiHeader.ipiSignature,"PCD_IPI") != 0) && !overview)
	{
		free(pcdFile);
		pcdFile = NULL;
		errorString = "That is not a valid PCD file";
		return false;
	}

	if (pcdFile->iciBase16.interleaveRatio != 1)
	{
		// We have interleaved audio......
		free(pcdFile);
		pcdFile = NULL;
		errorString = "The file contains interleaved audio";
		return false;
	}

	imageRotate = static_cast<PCDOrientation>(pcdFile->iciBase16.attributes & 0x03);
	imageResolution = static_cast<PCDResolution>(((pcdFile->iciBase16.attributes >> 2) & 0x03) + static_cast<int>(PCDResolution::Base));
	imageIPEAvailable = (pcdFile->iciBase16.attributes >> 4) & 0x01;
	imageHuffmanClass = (pcdFile->iciBase16.attributes >> 5) & 0x02;
	off_t base4Stop = read_be<uint16_t>(pcdFile->iciBase16.sectorStop4Base);
	// Calculate the file locations that are based of variable sized data
	// See the file decription above for why the calculation values
	HCTOffset[Base_x16] = base4Stop + 12;
	ICDOffset[Base_x16] = base4Stop + 14;
	// unused in this implementation
//	size_t base16Stop = read_be<uint16_t>(pcdFile->iciBase16.sectorStop16Base);
//	size_t ipeStop = read_be<uint16_t>(pcdFile->iciBase16.sectorStopIPE);

	return true;
}

bool pcdDecode::readImage (const fs::path ipe_file, PCDResolution maxScene)
{
	int Base_x4 = static_cast<int>(PCDResolution::Base_x4);
	int Base_x16 = static_cast<int>(PCDResolution::Base_x16);

	errorString.clear();

	if (!fh.is_open()) {
		errorString = "File is not open.";
		return false;
	}

	scene = maxScene;
	// Limit the resolution to what we have available
	if (imageResolution < PCDResolution::Base_x16) {
		scene = min(scene, imageResolution);
	}

	// This reads in the base image - may be the right size, may be smaller
	// if smaller, we need to get delta images.........
	baseScene = readBaseImage(fh, scene, ICDOffset, luma, chroma1, chroma2);

	// Test Image only
//	 genTestBaseImage(scene, luma, chroma1, chroma2);

	// Set for what we got now.......
	if (baseScene < PCDResolution::Base_div16) {
		// We couldn't find any image at all
		errorString = "No valid base image could be found";
		return false;
	}
	if (baseScene < PCDResolution::Base) {
		// The image was less than base resolution.....
		// so no deltas can be read
		scene = baseScene;
	}

	if (scene >= PCDResolution::Base_x4) {
		uint8_t ** deltas_x4 = deltas[Base_x4 - Base_x4];
		try {
			// Here we're reading in the 1536 by 1024 image's deltas - luma only
			// So we end up with an image with the chroma subsampled by a factor of 4
			ReadBuffer hufBuffer;
			huffTables *hTables = (huffTables *) malloc(sizeof(huffTables));
			if (hTables == NULL) {
				errorString = "Could not allocate huffman tables";
			}
			else {
				readAllHuffmanTables(fh, kSceneSectorSize * HCTOffset[Base_x4], hTables, 1);
				// Now we need to get the actual data......
				fh.seekg(kSceneSectorSize * ICDOffset[Base_x4], std::ios_base::beg);
				PCDSceneMetadata Base_x4_meta = PCDScenes.at(PCDResolution::Base_x4);
				deltas_x4[0] = (uint8_t *) malloc(Base_x4_meta.lumaWidth * Base_x4_meta.lumaHeight * sizeof(uint8_t));
				initReadBuffer(hufBuffer, fh);
				readPCDDeltas(hufBuffer, hTables, PCDResolution::Base_x4, 0, 0, deltas_x4, 0);

				if (scene >= PCDResolution::Base_x16) {
					uint8_t ** deltas_x16 = deltas[Base_x16 - Base_x4];
					try {
						// Here we're reading in the 3072 by 2048 image's deltas - luma and chroma
						// Chroma is subsampled by a factor of two. Aka 16 times more data than
						// the 4 Base image
						readAllHuffmanTables(fh, kSceneSectorSize * HCTOffset[Base_x16], hTables, monochrome ? 1 : 3);
						fh.seekg(kSceneSectorSize * ICDOffset[Base_x16], std::ios_base::beg);
						PCDSceneMetadata Base_x16_meta = PCDScenes.at(PCDResolution::Base_x16);
						deltas_x16[0] = (uint8_t *) malloc(Base_x16_meta.lumaWidth * Base_x16_meta.lumaHeight * sizeof(uint8_t));
						if (!monochrome) {
							deltas_x16[1] = (uint8_t *) malloc(Base_x16_meta.chromaWidth * Base_x16_meta.chromaHeight * sizeof(uint8_t));
							deltas_x16[2] = (uint8_t *) malloc(Base_x16_meta.chromaWidth * Base_x16_meta.chromaHeight * sizeof(uint8_t));
						}
						initReadBuffer(hufBuffer, fh);
						readPCDDeltas(hufBuffer, hTables, PCDResolution::Base_x16, 0, 0, deltas_x16, 0);
						if (scene >= PCDResolution::Base_x64) {
							// the 6144 by 4096 image;
							// parseICFile has its own internal try/catch
							if (!parseICFile(ipe_file)) {
								scene = PCDResolution::Base_x16;
								if (errorString.length() == 0)
									errorString = "Error while processing 64Base image";
							 }
						}
					}
					catch (char *err) {
						scene = PCDResolution::Base_x4;
						if (errorString.length() == 0)
							errorString = std::string(err) + " while processing 16Base image";
					}
					catch (...) {
						scene = PCDResolution::Base_x4;
						if (errorString.length() == 0)
							errorString = "Could not find a valid 16Base image; falling back to 4Base";
					}
					if (scene == PCDResolution::Base_x4) {
						if (deltas_x16[0] != NULL) {
							free (deltas_x16[0]);
							deltas_x16[0] = NULL;
						}
						if (deltas_x16[1] != NULL) {
							free (deltas_x16[1]);
							deltas_x16[1] = NULL;
						}
						if (deltas_x16[2] != NULL) {
							free (deltas_x16[2]);
							deltas_x16[2] = NULL;
						}
					}
				}
				free(hTables);
			}
		}
		catch (char *err) {
			scene = PCDResolution::Base;
			if (errorString.length() == 0)
				errorString = std::string(err) + " while processing 4Base image";
		}
		catch (...) {
			scene = PCDResolution::Base;
			if (errorString.length() == 0)
				errorString = "Could not find a valid 4Base image; falling back to Base";
		}
		if (scene == PCDResolution::Base) {
			if (deltas_x4[0] != NULL) {
				free (deltas_x4[0]);
				deltas_x4[0] = NULL;
			}
		}
	}

	fh.close();
	return true;
}
