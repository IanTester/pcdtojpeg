/* =======================================================
 * pcdDecode - a Photo-CD image decoder and converter
 * =======================================================
 *
 * Project Info:  http://sourceforge.net/projects/pcdtojpeg/
 * Project Lead:  Sandy McGuffog (sandy.cornerfix@gmail.com);
 *
 * (C) Copyright 2009-2011, by Sandy McGuffog and Contributors.
 *
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * ---------------
 * See pcdDecode.hh for more information
 */

#pragma once

#include <string>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <math.h>
#include "types.hh"
#include "tables.hh"
#include "InterpolatedTable.hh"
#include "CubicCurve.hh"

//////////////////////////////////////////////////////////////
//
// Support code for performance analysis - Mac only
//
//////////////////////////////////////////////////////////////
#ifdef ___PerformanceAnalysis
#ifdef qMacOS
float HowLong(AbsoluteTime endTime,
		     AbsoluteTime bgnTime)
{
    AbsoluteTime absTime;
    Nanoseconds  nanosec;

    absTime = SubAbsoluteFromAbsolute(endTime, bgnTime);
    nanosec = AbsoluteToNanoseconds(absTime);
    return (float) UnsignedWideToUInt64( nanosec ) / 1000.0;
}
#endif
#endif

//////////////////////////////////////////////////////////////
//
// Big-endian reads
//
//////////////////////////////////////////////////////////////
//
template <typename T>
T read_be(uint8_t* buffer);

//////////////////////////////////////////////////////////////
//
// Utility String functions
//
//////////////////////////////////////////////////////////////
//

int compareBytes(const char *buffer, const char *string);
void copyWithoutPadding(std::string& dest, const char* src, unsigned int length);
std::string BCD(uint8_t byte, bool pad = true);


//////////////////////////////////////////////////////////////
//
// Huffman decoder implementation
// This is a standard table-based Huffman decoder
//
//////////////////////////////////////////////////////////////

void readHuffTable(struct hctTable& source, struct huffTable& destination, int& number);
int readNextSector(ReadBuffer& buffer);
void PCDGetBits(ReadBuffer& b, int n);
void initReadBuffer(ReadBuffer& buffer, std::istream& file);
void syncHuffman(ReadBuffer& b);
void PCDDecodeHuffman(ReadBuffer& b, struct huffTable& huf, uint8_t *dest, int length);
void readAllHuffmanTables(std::istream& fh, off_t offset, huffTables* tables, int numTables);


//////////////////////////////////////////////////////////////
//
// Reader for the delta tables - this supports base, 16 base
// and 64Base
//
//////////////////////////////////////////////////////////////

bool readPCDDeltas(ReadBuffer& buf, struct huffTables* huf, PCDResolution sceneSelect, int sequenceSize, int sequencesToProcess, uint8_t *data[3], off_t colOffset);


//////////////////////////////////////////////////////////////
//
// Interpolation routines
//
//////////////////////////////////////////////////////////////
template <typename T>
T min(T x, T y) {
  return x < y ? x : y;
}

template <typename T>
T max(T x, T y) {
  return x > y ? x : y;
}

template <typename T>
T pin(T low, T x, T high) {
  return x < low ? low
    : x > high ? high
    : x;
}


//////////////////////////////////////////////////////////////
//
// basic "Kodak standard" bilinear upres interpolator
//
//////////////////////////////////////////////////////////////
void upResInterpolate(uint8_t *base, uint8_t *dest, uint8_t *luma, unsigned int width, unsigned int height, bool hasDeltas);


void upResBuffer(uint8_t *base, uint8_t *dest, uint8_t *luma, unsigned int width, unsigned int height, PCDUpResMethod upResMethod, bool hasDeltas);


//////////////////////////////////////////////////////////////
//
// Test code
//
//////////////////////////////////////////////////////////////

#ifdef DEBUG
void dump8by8(uint8_t *b, int width);
void dumpColumn(uint8_t *b, int col, int height, int width);
void genTestBaseImage(PCDResolution scene, uint8_t *luma, uint8_t *chroma1, uint8_t *chroma2);

#endif


size_t Width(PCDResolution res, PCDOrientation rot = PCDOrientation::Rot0);
size_t Height(PCDResolution res, PCDOrientation rot = PCDOrientation::Rot0);

//////////////////////////////////////////////////////////////
//
// The Micro CMM
//
//////////////////////////////////////////////////////////////

extern CubicHermite boostCurve;

// Setup our hermite curve so that it maps [0, 1.3584] => [0, 1].
// The slopes at the beginning and end are changed depending on the 'boost'.
inline void setup_boost(float val) {
  boostCurve.set_hermite_parameters(1.3584,
				    0,			// p0
				    1,			// p1
				    1 + (0.3584 * val),	// m0
				    1 / (1 + val));	// m1
}


void toLinear(v4f* row, size_t columns);
void tosRGB(v4f* row, size_t columns);

template <typename T>
void output_to(v4f* row, size_t columns, T *red, T *green, T *blue, T *alpha, ptrdiff_t destInc);


// Normally, what we would do is to define a Photo CD color space, then
// hand that together with the data to a Color Management Module - e.g.,
// LittleCMS - and let it deal with all the nasty complex color space conversions.
// However, to keep this as standalone as possible, what is implemented here is
// a "micro CMM". It does everything that a full CMM would do.....admittedly,
// only for the limited Photo CD to linear light to sRGB conversions that we need.
//
template <typename T>
void convertToRGB(T *red, T *green, T *blue, T *alpha,
		  ptrdiff_t step, size_t columns, size_t rows,
		  uint8_t *lp, uint8_t *c1p, uint8_t *c2p,
		  unsigned int resFactor, PCDOrientation imageRotate, PCDColorSpace colorSpace, PCDWhiteBalance whiteBalance,
		  std::function<void(size_t)> row_cb = nullptr)
{
	static const v4f zero_v = { 0, 0, 0, 0}, one_v = { 1, 1, 1, 1 };
	size_t resMask = (1 << resFactor) - 1;

#pragma omp parallel for schedule(dynamic, 1)
	for (size_t row = 0; row < rows; row++) {
		ptrdiff_t destIndex, destInc;
		switch (imageRotate) {
			case PCDOrientation::Rot0:
				destIndex = row * columns * step;
				destInc = step;
				break;
			case PCDOrientation::Rot90:
				destIndex = (row + (columns - 1) * rows) * step;
				destInc = -rows * step;
				break;
			case PCDOrientation::Rot180:
				destIndex = (columns - 1 + (rows - 1 - row) * columns) * step;
				destInc = -step;
				break;
			case PCDOrientation::Rot270:
				destIndex = (rows - 1 - row) * step;
				destInc = rows * step;
				break;
			default:
				destIndex = 0;
				destInc = 0;
				break;
		}

		ptrdiff_t lumaIndex = row * columns;
		ptrdiff_t chromaIndex = (row >> resFactor) * (columns >> resFactor);

		uint8_t *lp_pix = lp + lumaIndex, *c1p_pix = c1p + chromaIndex, *c2p_pix = c2p + chromaIndex;

		v4f floatRow[columns], *floatPix;
		if (colorSpace == PCDColorSpace::YCC) {
			// Here we want the original YCC color space
			floatPix = floatRow;
			for (size_t col = 0; col < columns; col++, floatPix++) {
			  v4f ycc = { (float)*lp_pix, (float)*c1p_pix, (float)*c2p_pix };
				ycc = pin(zero_v, ycc * (1 / 255.0f), one_v);
				*floatPix = ycc;

				lp_pix++;
				if ((col & resMask) == resMask) {
				  c1p_pix++;
				  c2p_pix++;
				}
			}
		}
		else {
			// here one or the other of the RGB color spaces
			floatPix = floatRow;
			for (size_t col = 0; col < columns; col++, floatPix++) {
				float L = *lp_pix * 1.3584;
				float C1 = 0, C2 = 0;
				if (c1p != NULL)
					C1 = ((int32_t)*c1p_pix - 156) * 2.2179;

				if (c2p != NULL)
					C2 = ((int32_t)*c2p_pix - 137) * 1.8215;

				v4f chroma = { C2, (0.194f * C1) - (0.509f * C2), C1 };
				v4f rgb = pin(zero_v, boostCurve((L + chroma) * (1 / 255.0f)), one_v);
				*floatPix = rgb;

				lp_pix++;
				if ((col & resMask) == resMask) {
				  c1p_pix++;
				  c2p_pix++;
				}
			}

			// Here we have RGB in the original photo CD color space. So we can either
			// (a) pass that back raw, or
			// (b) convert to a CCIR709 linear light space, or
			// (c) convert to a sRGB space
			if ((colorSpace == PCDColorSpace::LinearCCIR709) || (colorSpace == PCDColorSpace::sRGB)) {
				toLinear(floatRow, columns);
				// We only do whitebalance conversions for the processed spaces, not raw.....
				if (whiteBalance == PCDWhiteBalance::D50) {
					// http://www.brucelindbloom.com/index.html?Eqn_ChromAdapt.html
					// D50 to D65, Bradford method
					static const v4f matrix[3] = { {  0.9555766, -0.0282895,  0.0122982 },
								       { -0.0230393,  1.0099416, -0.0204830 },
								       {  0.0631636,  0.0210077,  1.3299098 } };
					floatPix = floatRow;
					for (size_t col = 0; col < columns; col++, floatPix++) {
						v4f rgb = (((*floatPix)[0] * matrix[0])
							   + ((*floatPix)[1] * matrix[1])
							   + ((*floatPix)[2] * matrix[2]));
						*floatPix = rgb;
					}
				}
			}
			if (colorSpace == PCDColorSpace::sRGB) {
				// Recompress
				tosRGB(floatRow, columns);
			}
			else {
				// just pin
				floatPix = floatRow;
				for (size_t col = 0; col < columns; col++, floatPix++) {
					v4f rgb = pin(zero_v, *floatPix, one_v);
					*floatPix = rgb;
				}
			}

			// Deliver back in the right format
			output_to<T>(floatRow, columns, red + destIndex, green + destIndex, blue + destIndex, alpha == nullptr ? nullptr : alpha + destIndex, destInc);
		}
		if (row_cb != nullptr)
			row_cb(row);
	}
}


//////////////////////////////////////////////////////////////
//
// Base (and lower) image reader
//
//////////////////////////////////////////////////////////////

PCDResolution readBaseImage(std::istream& fp, PCDResolution scene, int ICDOffset[MaxPCDScenes], uint8_t*& luma, uint8_t*& chroma1, uint8_t*& chroma2);
