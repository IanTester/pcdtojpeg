/* =======================================================
 * pcdtojpeg - a Photo-CD to JPEG image format converter
 * =======================================================
 *
 * Project Info:  http://sourceforge.net/projects/pcdtojpeg/
 * Project Lead:  Sandy McGuffog (sandy.cornerfix@gmail.com);
 *
 * (C) Copyright 2009-2011, by Sandy McGuffog and Contributors.
 *
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * ---------------
 * main.cpp
 * ---------------
 * (C) Copyright 2009-2013, by Sandy McGuffog and Contributors.
 *
 * Original Author:  Sandy McGuffog;
 * Contributor(s):   -;
 *
 * Acknowlegements:   Hadmut Danisch (danisch@ira.uka.de), who authored hpcdtoppm in the early
 *                    90's. Although this software itself is unrelated to hpcdtoppm, the pcdDecoder
 *                    package would not have been possible without Hadmut's original reverse
 *                    engineering of the PCD file format.
 *
 *                    Ted Felix (http://tedfelix.com/ ), who provided sample PCD images and assorted
 *                    PCD documentation, as well as testing early versions of this software
 *
 * Changes
 * -------
 *
 * V1.0.2 - 1 July 2009 - Added YCC color space to the decoder library
 *                        Improved sRGB shadow detail and color handling
 *
 * V1.0.3 - 1 Sept 2009 - V1.0.3 decoder library
 *                        Even more path separator finding code
 *                        Enhanced 64Base IPE file location algorithm
 *                        More descriptive error messages
 * V1.0.4 - 19 Sept 2009 - Multithreading under Windows
 *
 * V1.0.5 - 21 Sept 2009 - More efficient memory management
 *
 * V1.0.7 - 12 Feb 2010 - New decoder library version
 *
 * V1.0.8 - 20 Mar 2010 - New decoder library version
 *
 * V1.0.9 - 23 Sept 2010 - New decoder library version
 *
 * V1.0.10 - 3 April 2011 - New decoder library version
 *
 * V1.0.11 - 3 Jan 2013 - Clean some includes for linux
 *
 * V1.0.12 - 24 April 2015 - New decoder library version, clean up includes
 *
 */

#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include <jpeglib.h>
#include <lcms2.h>
#include "pcdDecode.hh"

#if defined(_WIN32) || defined(_WIN32_) || defined(__WIN32__) || defined(WIN32) || defined(_MSC_VER) || defined(__CYGWIN__) || defined(__MINGW32__) || defined(__BORLANDC__)
#define pcdHaveWinOS 1
#include <direct.h>
#else
#include <unistd.h>
#endif

struct jpeg_destination_state_t {
	JOCTET *buffer;
	std::ostream *os;
	size_t buffer_size;
};

static void jpeg_ostream_init_destination(j_compress_ptr cinfo) {
	jpeg_destination_mgr *dmgr = (jpeg_destination_mgr*)(cinfo->dest);
	jpeg_destination_state_t *ds = (jpeg_destination_state_t*)(cinfo->client_data);
	ds->buffer = new JOCTET[ds->buffer_size];
	if (ds->buffer == NULL)
		throw "Out of memory?";

	dmgr->next_output_byte = ds->buffer;
	dmgr->free_in_buffer = ds->buffer_size;
}

static boolean jpeg_ostream_empty_output_buffer(j_compress_ptr cinfo) {
	jpeg_destination_mgr *dmgr = (jpeg_destination_mgr*)(cinfo->dest);
	jpeg_destination_state_t *ds = (jpeg_destination_state_t*)(cinfo->client_data);
	ds->os->write((char*)ds->buffer, ds->buffer_size);
	dmgr->next_output_byte = ds->buffer;
	dmgr->free_in_buffer = ds->buffer_size;
	return 1;
}

static void jpeg_ostream_term_destination(j_compress_ptr cinfo) {
	jpeg_destination_mgr *dmgr = (jpeg_destination_mgr*)(cinfo->dest);
	jpeg_destination_state_t *ds = (jpeg_destination_state_t*)(cinfo->client_data);
	ds->os->write((char*)ds->buffer, ds->buffer_size - dmgr->free_in_buffer);
	delete [] ds->buffer;
	ds->buffer = NULL;
	dmgr->free_in_buffer = 0;
}

void jpeg_ostream_dest(j_compress_ptr cinfo, std::ostream* os) {
	jpeg_destination_state_t *ds = new jpeg_destination_state_t;
	ds->os = os;
	ds->buffer_size = 1048576;
	cinfo->client_data = (void*)ds;

	jpeg_destination_mgr *dmgr = new jpeg_destination_mgr;
	dmgr->init_destination = jpeg_ostream_init_destination;
	dmgr->empty_output_buffer = jpeg_ostream_empty_output_buffer;
	dmgr->term_destination = jpeg_ostream_term_destination;
	cinfo->dest = dmgr;
}

void jpeg_ostream_dest_free(j_compress_ptr cinfo) {
	delete (jpeg_destination_state_t*)cinfo->client_data;
	delete (jpeg_destination_mgr*)cinfo->dest;
}

void jpeg_write_profile(jpeg_compress_struct* cinfo, unsigned char *data, unsigned int size) {
	unsigned char num_markers = ceil(size / 65519.0); // max 65533 bytes in a marker, minus 14 bytes for the ICC overhead

	unsigned int data_left = size;
	for (unsigned char i = 0; i < num_markers; i++) {
		int marker_data_size = data_left > 65519 ? 65519 : data_left;
		JOCTET *APP2 = new JOCTET[marker_data_size + 14];
		memcpy(APP2, "ICC_PROFILE\0", 12);
		APP2[12] = i + 1;
		APP2[13] = num_markers;
		memcpy(APP2 + 14, data, marker_data_size);

		jpeg_write_marker(cinfo, JPEG_APP0 + 2, APP2, marker_data_size + 14);
		data += marker_data_size;
		data_left -= marker_data_size;
	}
}

//////////////////////////////////////////////////////////////
//
// JPEG compression
// Uses the Independent JPEG Group's library: http://www.ijg.org/
// The code was mostly writtem by Tom Lane.
//
//////////////////////////////////////////////////////////////
// write_JPEG_file is mostly a direct copy from the ijg's sample code, but
// adds injecting a sRGB profile into the file. This is done without using a
// CMM such as LittleCMS so as to minimise external dependencies

void write_JPEG_file (fs::path filename,
			  int quality,
			  JSAMPLE * image_buffer,
			  int image_height,
			  int image_width)
{
	// This struct contains the JPEG compression parameters and pointers to
	// working space (which is allocated as needed by the JPEG library).
	jpeg_compress_struct cinfo;

	// This struct represents a JPEG error handler.
	jpeg_error_mgr jerr;
	// The outfile and data pointers
	std::ofstream outfile;
	JSAMPROW row_pointer[1];
	int row_stride;

	// Allocate and initialize JPEG compression object
	cinfo.err = jpeg_std_error(&jerr);
	jpeg_create_compress(&cinfo);

	// Specify data destination (our file)
	outfile.open(filename.native(), std::ios_base::out | std::ios_base::binary);
	if (outfile.fail()) {
		fprintf(stderr, "can't open %s\n", filename.c_str());
		exit(1);
	}
	jpeg_ostream_dest(&cinfo, &outfile);

	// Set parameters for compression
	// Note that the JPEG library's "color_space" isn't actually a color space;
	// it's really just a data format setting. The library has no knowledge of
	// ICC type color spaces.
	cinfo.image_width = image_width;
	cinfo.image_height = image_height;
	// We will ask pcdDecoder for RGB, no alpha
	cinfo.input_components = 3;
	// 8-bit RGB
	cinfo.in_color_space = JCS_RGB;

	// Now use the library's routine to set default compression parameters.
	jpeg_set_defaults(&cinfo);

	// Set the quality, and specify baseline JPEG to maximise compatibility
	jpeg_set_quality(&cinfo, quality, TRUE);

	// Start compressor, specifying a complete interchange-JPEG file
	jpeg_start_compress(&cinfo, TRUE);

	cmsHPROFILE sRGB = cmsCreate_sRGBProfile();
	cmsUInt32Number sRGB_size;
	if (cmsSaveProfileToMem(sRGB, NULL, &sRGB_size)) {
	  unsigned char *sRGB_data = (unsigned char*)malloc(sRGB_size);
		if (sRGB_data != nullptr) {
			if (cmsSaveProfileToMem(sRGB, sRGB_data, &sRGB_size))
				jpeg_write_profile(&cinfo, sRGB_data, sRGB_size);

			free(sRGB_data);
		}
	}

	// Write the actual scanlines
	row_stride = image_width * 3;
	while (cinfo.next_scanline < cinfo.image_height) {
		row_pointer[0] = & image_buffer[cinfo.next_scanline * row_stride];
		(void) jpeg_write_scanlines(&cinfo, row_pointer, 1);
	}

	// Finish compression and close the file
	jpeg_finish_compress(&cinfo);
	outfile.close();
	jpeg_ostream_dest_free(&cinfo);

	// Release JPEG compression object
	jpeg_destroy_compress(&cinfo);
}


//////////////////////////////////////////////////////////////
//
// The main program
//
//////////////////////////////////////////////////////////////
// All this does is to parse the command line, pass the file to the decoder,
// apply brightness if requested, then write out the file as a JPEG

#define kpcdtojpegVersion "1.0.12"

void printName()
{
	fprintf (stderr,
			 "\n"
			 "pcdtojpeg, version " kpcdtojpegVersion " "
			 "\n"
			 "Copyright (C) 2009-2015 Sandy McGuffog\n"
			 "JPEG compression code Copyright (C) 1991-1998, Thomas G. Lane.\n");
}

void printUsage(char * const argv [], bool isVerbose)
{
	if (!isVerbose) printName();
	fprintf (stderr,
			 "\n"
			 "Usage:  %s [options] file1 [file2]\n"
			 "\n"
			 "Valid options:\n"
			 "-h            Print this message\n"
			 "-v            Verbose file information\n"
			 "-m            Process the file as monochrome\n"
			 "-D50          Process for a white balance of D50\n"
			 "-D65          Process for a white balance of D65 <default>\n"
			 "-q nnn        JPEG file quality (nnn range 1 to 100 <100>)\n"
			 "-b n.n        Brightness adjustment (n.n range -0.5 to 7.0 <0.0>)\n"
			 "-r n          Highest resolution to extract (n range 0 to 5):\n"
			 "                 0 - Base/16 (128 x 192)\n"
			 "                 1 - Base/4 (256 x 384)\n"
			 "                 2 - Base (512 x 768)\n"
			 "                 3 - 4Base (1024 x 1536)\n"
			 "                <4 - 16Base (2048 x 3072)>\n"
			 "                 5 - 64Base (4096 x 6144)\n"
			 "\n",
			 argv [0]);
}

int main (int argc, char * const argv[]) {
    // parse the arguments
	bool doneWithArguments = false;
	int argIndex = 1;
	bool isVerbose = false;
	bool isMonochrome = false;
	bool isD50White = false;
	int jpegQuality = 100;
	float boost = 0.0f;
	PCDResolution resolution = PCDResolution::Base_x16;
	size_t width, height;
	pcdDecode *decoder;
	fs::path outFile, iceFile;

	if (argc < 2) {
		printUsage(argv, isVerbose);
		exit(0);
	}
	while ((!doneWithArguments) && (argIndex < argc)) {
		std::string thisArg(argv[argIndex]);
		if (thisArg[0] == '-') {
			if ((thisArg == "-h") || (thisArg == "-H")) {
				printUsage(argv, isVerbose);
				exit(0);
			}
			else if (thisArg == "-v") {
				isVerbose = true;
				printName();
			}
			else if (thisArg == "-m") {
				isMonochrome = true;
			}
			else if (thisArg == "-D50") {
				isD50White = true;
			}
			else if (thisArg == "-D65") {
				isD50White = false;
			}
			else if (thisArg == "-q") {
				if (argIndex > (argc - 2)) {
					printUsage(argv, isVerbose);
					exit(-1);
				}
				jpegQuality = atoi(argv[argIndex+1]);
				jpegQuality = jpegQuality > 100 ? 100 : jpegQuality;
				jpegQuality = jpegQuality < 0 ? 0 : jpegQuality;
				argIndex++;
			}
			else if (thisArg == "-b") {
				if (argIndex > (argc - 2)) {
					printUsage(argv, isVerbose);
					exit(-1);
				}
				boost = (float) atof(argv[argIndex+1]);
				if (boost < -0.5)
					boost = -0.5;
				else if (boost > 7)
					boost = 7;
				argIndex++;
			}
			else if (thisArg == "-r") {
				if (argIndex > (argc - 2)) {
					printUsage(argv, isVerbose);
					exit(-1);
				}
				int r = atoi(argv[argIndex+1]);
				if (r < 0)
				  r = 0;
				if (r > 5)
				  r = 5;
				resolution = static_cast<PCDResolution>(r);
				argIndex++;
			}
			else {
				fprintf (stderr, "Invalid argument\n");
				printUsage(argv, isVerbose);
				exit(-1);
				}
			argIndex++;
		}
		else {
			doneWithArguments = true;
		}
	}
	// Now check we have file[s]
	if (argIndex > (argc - 1)) {
		fprintf (stderr, "Invalid argument\n");
		printUsage(argv, isVerbose);
		exit(-1);
	}

	// Let's see if we can actually find this file
	// Of course, windows can't even have a normal stat function.....

#if defined(pcdHaveWinOS)
	struct _stat stFileInfo;
	if (_stat(argv[argIndex], &stFileInfo) == -1) {
#else
	struct stat stFileInfo;
	if (stat(argv[argIndex], &stFileInfo) == -1) {
#endif
		fprintf (stderr, "pcdtojpeg could not find the file \"%s\" - check the name you entered\n", argv[argIndex]);
		exit(-1);
	}
	// Get us a decoder
	decoder = new pcdDecode();
	if (decoder == NULL) {
		fprintf (stderr, "Could not create a decoder - probably too little memory\n");
		exit(-1);
	}
	// Set to the best possible quality interpolation; if this is the the GPL decoder,
	// it doesn't actually have the LumaIterpolate, but will automatically fall
	// back to the best it has
	decoder->setInterpolation(PCDUpResMethod::LumaIterpolate);

	if (resolution > PCDResolution::Base_x16) {
		// We need to generate the location of the IC file.
		// Note this only works if the directory structure is the same as the original CD
		// Also note that here, if there isn't a path separator in the path, we can't find
		// the file anyway, so this is an ok way to decide what separator to use.
		iceFile = fs::absolute(argv[argIndex]);

		std::string nameOnly = iceFile.filename().native();
		size_t loc = nameOnly.find_last_of(".");
		if (loc != std::string::npos)
			nameOnly.erase(loc);
		bool useLowerCase = (nameOnly.find_last_of("img") != std::string::npos);

		iceFile = iceFile.parent_path()
		  / (useLowerCase ? "ipe" : "IPE")
		  / nameOnly
		  / (useLowerCase ? "64base" : "64BASE")
		  / (useLowerCase ? "info.ic" : "INFO.IC");
	}

	// If we want monochrome, now is the time to ask for it
	decoder->setIsMonoChrome(isMonochrome);

	// If we want D50, now is the time to ask for it
	decoder->setWhiteBalance(isD50White ? PCDWhiteBalance::D50 : PCDWhiteBalance::D65);

	// Parse the file header
	if (!decoder->parseHeader(argv[argIndex])) {
		// false here means there isn't any kind of a valid image
		fprintf (stderr, "Decoder Error: %s\n", decoder->getErrorString().c_str());
		delete (decoder);
		if (resolution > PCDResolution::Base_x16) {
			fprintf (stderr, " while trying to process ICE file \"%s\"\n", iceFile.c_str());
		}
		exit(-1);
	}

	// Read image data
	if (!decoder->readImage(iceFile, resolution)) {
		// false here means there isn't any kind of a valid image
		fprintf (stderr, "Decoder Error: %s\n", decoder->getErrorString().c_str());
		delete (decoder);
		if (resolution > PCDResolution::Base_x16) {
			fprintf (stderr, " while trying to process ICE file \"%s\"\n", iceFile.c_str());
		}
		exit(-1);
	}

	// Even if we got a valid image back, there may be warnings - let's print those:
	if (decoder->getErrorString().length() > 0) {
		fprintf (stderr, "Warning: %s\n", decoder->getErrorString().c_str());
		if (resolution > PCDResolution::Base_x16) {
			fprintf (stderr, " while trying to process ICE file \"%s\"\n", iceFile.c_str());
		}
	}

	// At this point we have all the metadata, so we can take decisions based on that
	// (e.g., resolution, original medium) if that's what we want
	// But the data is still in its component pieces as in the original file, so we can't
	// call any of the populateBuffer routines
	if (isVerbose) {
		std::string descrip, val;
		printf("Image metadata:\n");
		for (int i = 0; i < MaxPCDMetadata; i++) {
			decoder->getMetadata(static_cast<PCDMetaDataDictionary>(i), descrip, val);
			printf("  %s: %s\n", descrip.c_str(), val.c_str());
		}
	}

	// We might not actually have gotten an image at the resolution we asked for,
	// so get the size of what we have got.....
	width = decoder->getWidth();
	height = decoder->getHeight();
	if (isVerbose) {
		printf("Image size: %d x %d\n", (int) decoder->getWidth(), (int) decoder->getHeight());
	}

	// Now we post parse. This assembles all the various pieces of base and residual
	// image data into a single YCC format image. This operation is multi-threaded,
	// if multi-threading is enabled in the decoder library
	decoder->postParse();

	// sRGB is by far the most widely accepted color space, so set up for that
	decoder->setColorSpace(PCDColorSpace::sRGB);

	setup_boost(boost);

	size_t nBytes = width * height * 3 * sizeof(uint8_t);
	uint8_t *table = (uint8_t *) malloc(nBytes);
	if (table != NULL) {
		// Now we call populateBuffers. This converts the YCC data (that postParse assembled)
		// into RGB in the choice of formats. This operation is multi-threaded, if
		// multi-threading is enabled in the decoder library
		decoder->populateBuffers<uint8_t>(&(table[0]), &(table[1]), &(table[2]), NULL, 3);
	}
	else {
		fprintf (stderr, "Could not allocate memory for the JPEG conversion\n");
		delete (decoder);
		exit(-1);
	}
	// We can free the decoder's memory now, as we have the buffer full of RGB data
	delete (decoder);
	decoder = NULL;

	// If an output file wasn't specified, synthesize a filename
	if (argIndex < (argc-1)) {
		outFile = argv[argIndex+1];
	}
	else {
		// Create a new filename
		std::string baseFile(argv[argIndex]);
		size_t loc = baseFile.find_last_of(".");
		// Make sure there is enough room in the string, because the standard
		// C++ string implementation is too dumb to do so.....
		baseFile.resize(baseFile.size()+3);
		if (loc != std::string::npos) {
			baseFile = baseFile.erase(loc);
		}
		baseFile = baseFile.append(".jpeg");
		outFile = baseFile;
	}

	// Now we just compress the buffer into a JPEG format file, courtesy of Thomas G.
	// Lane's JPEG library, and also add the sRGB profile.
	// If we don't add the profile, then all our hard work in the decoder to keep the
	// color space straight goes to waste.....
	write_JPEG_file (outFile,
					 jpegQuality,
					 table,
					 (int) height,
					 (int) width);

	free(table);
	table = NULL;

    return 0;
}
