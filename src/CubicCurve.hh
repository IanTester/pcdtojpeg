/*
        Copyright 2017 Ian Tester

        This file is part of pcdtojpeg.

        pcdtojpeg is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        pcdtojpeg is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with pcdtojpeg.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

//! Simple cubic curve base class
class CubicCurve {
private:
  float _a, _b, _c, _d;

public:
  //! Empty constructor with zero coefficients: f(x) = 0
  CubicCurve() :
    _a(0), _b(0), _c(0), _d(0)
  {}

  //! Constructor with coefficients
  CubicCurve(float a, float b, float c, float d) :
    _a(a), _b(b), _c(c), _d(d)
  {}

  //! Set the raw coefficients of the curve
  void set_coefficients(float a, float b, float c, float d) {
    _a = a;
    _b = b;
    _c = c;
    _d = d;
  }

  //! Evaluate the curve at a point
  inline float operator ()(float x) const {
    return (((((_d * x) + _c) * x) + _b) * x) + _a;	// 3 mults, 3 adds
  }

  //! Vector evaluation of the curve
  inline v4f operator ()(v4f x) const {
    return (((((_d * x) + _c) * x) + _b) * x) + _a;
  }

};

class CubicHermite : public CubicCurve {
public:
  //! Empty constructor
  CubicHermite() :
    CubicCurve()
  {}

  //! Constructor with Hermite parameters
  /*!
    \param p0 Value at x=0
    \param p1 Value at x=1
    \param m0 Slope at x=0
    \param m1 Slope at x=1
  */
  CubicHermite(float p0, float p1, float m0, float m1) :
    CubicCurve(p0,
	       m0,
	       (-3 * p0) - (2 * m0) + (3 * p1) - m1,
	       (2 * p0) + m0 - (2 * p1) + m1)
  {}

  //! Set Hermite parameters
  void set_hermite_parameters(float p0, float p1, float m0, float m1) {
    set_coefficients(p0,
		     m0,
		     (-3 * p0) - (2 * m0) + (3 * p1) - m1,
		     (2 * p0) + m0 - (2 * p1) + m1);
  }

  //! A special method for setting parameters when x1 != 1
  void set_hermite_parameters(float x1, float p0, float p1, float m0, float m1) {
    set_coefficients(p0,
		     m0 / x1,
		     ((-3 * p0) - (2 * m0) + (3 * p1) - m1) / (x1 * x1),
		     ((2 * p0) + m0 - (2 * p1) + m1) / (x1 * x1 * x1));
  }

};
