/*
        Copyright 2017 Ian Tester

        This file is part of pcdtojpeg.

        pcdtojpeg is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        pcdtojpeg is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with pcdtojpeg.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <functional>
#include <type_traits>
#include "types.hh"

template <typename O, uint32_t size>
class InterpolatedTable {
private:
  uint32_t _size = size;
  float _min, _max, _rrange;
  O _table[size];

  typedef O VO __attribute__ ((vector_size (4 * sizeof(O))));

public:
  InterpolatedTable(std::function<O(float)> func, float min, float max) :
    _min(min), _max(max)
  {
    float range = max - min;
    _rrange = 1.0 / range;
    for (uint32_t i = 0; i < _size; i++) {
      float x = _min + (((float)i / (_size - 1)) * range);
      _table[i] = func(x);
    }
  }

  O operator ()(float x) {
    float fi = x < _min ? 0.0
      : x > _max ? 1.0
      : (_size - 1) * (x - _min) * _rrange;
    uint32_t i1 = floor(fi), i2 = ceil(fi);
    float t = fi - i1;

    return ((1 - t) * _table[i1]) + (t * _table[i2]);
  }

  VO operator ()(v4f x) {
    static const v4f zero_v = { 0, 0, 0, 0}, one_v = { 1, 1, 1, 1 };
    v4f fi = x < _min ? zero_v :
      x > _max ? one_v :
      (float)(_size - 1) * (x - _min) * _rrange;
    v4f i1 = { floor(fi[0]), floor(fi[1]), floor(fi[2]) };
    v4i i2 = { (uint32_t)ceil(fi[0]), (uint32_t)ceil(fi[1]), (uint32_t)ceil(fi[2]) };
    VO val1 = { _table[(uint32_t)i1[0]], _table[(uint32_t)i1[1]], _table[(uint32_t)i1[2]] };
    VO val2 = { _table[i2[0]], _table[i2[1]], _table[i2[2]] };

    v4f t = fi - i1;
    return ((1.0f - t) * val1) + (t * val2);
  }
};

