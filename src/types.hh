/* =======================================================
 * pcdDecode - a Photo-CD image decoder and converter
 * =======================================================
 *
 * Project Info:  http://sourceforge.net/projects/pcdtojpeg/
 * Project Lead:  Sandy McGuffog (sandy.cornerfix@gmail.com);
 *
 * (C) Copyright 2009-2011, by Sandy McGuffog and Contributors.
 *
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * ---------------
 * See pcdDecode.hh for more information
 */

#pragma once

#ifndef _MSC_VER
// So, of course, MS doesn't support stdint.h(!)
#include <stdint.h>
//#define pcdMagicAPI
#else
// So we have to define our own MS specific stuff
typedef __int8            int8_t;
typedef __int16           int16_t;
typedef __int32           int32_t;
typedef __int64           int64_t;
typedef unsigned __int8   uint8_t;
typedef unsigned __int16  uint16_t;
typedef unsigned __int32  uint32_t;
typedef unsigned __int64  uint64_t;
#endif

#if defined (_USRDLL) && defined (_MSC_VER)
#define pcdMagicAPI __declspec(dllexport)
#else
#define pcdMagicAPI
#endif

#include <fstream>

#ifdef qMacOS
#include <CoreServices/CoreServices.h>
#endif

//////////////////////////////////////////////////////////////
//
// Coding
//
//////////////////////////////////////////////////////////////
// Broadly, this code follows this as a standard:
//	clock_t represents the system times in clock ticks.
//	dev_t is used for device numbers.
//	off_t is used for file sizes and offsets.
//	ptrdiff_t is the signed integral type for the result of subtracting two pointers.
//	size_t reflects the size, in bytes, of objects in memory.
//	ssize_t is used by functions that return a count of bytes or an error indication.
//	time_t counts time in seconds.

#define KSectorSize 0x800
#define UseFourPixels 1

#ifdef DEBUG
	#define mInformPrintf 1
#endif

//////////////////////////////////////////////////////////////
//
// Performance Analysis
//
//////////////////////////////////////////////////////////////
// Define this to enable performance analysis on the Mac
#define ___PerformanceAnalysis 1
#ifdef DEBUG
	#define ___PerformanceAnalysis 1
#endif


//////////////////////////////////////////////////////////////
//
// Overall PCD file structure
// (in sector measures)
//
//////////////////////////////////////////////////////////////
//
// Name				Start			Length
// IP-ICA			0				1
// IPA				1				2
// Base/16 ICA			3				1
// Base/16 ICD			4				18
// Base/4 ICA			22				1
// Base/4 ICD			23				72
// Base ICA			95				1
// Base ICD			96				288
// 4Base ICA			384				1
// 4Base LPT-MRS		385				2
// 4Base LPT			387				1
// 4Base HCT			388				1
// 4Base ICD			389				variable
// 16Base ICA			var				1		Start defined by the base4Stop field in the
// 										ImageComponentAttributes
// 16Base LPT-MRS		var				9
// 16Base LPT			var				2
// 16Base HCT			var				2
// 16Base ICD			var				variable
//
// ICA - Image Component Attributes
// IPA - Image Pack Attributes
// ICI - Image Component Information
// ICD - Image Component Data (luma and interleaved chroma)
// LPT - Line Pointer Table
// HCT - Huffman Code Table


//////////////////////////////////////////////////////////////
//
// Private IPI structure definitions
//
//////////////////////////////////////////////////////////////
struct IPIHeader
{
	char ipiSignature[7];						// "PCD_IPI"
	uint8_t specificationVersion[2];				// Binary coded MSB major, LSB minor
	uint8_t authoringSoftwareRelease[2];				// Binary coded MSB major, LSB minor
	uint8_t imageMagnificationDescriptor[2];			// BCD coded MSB major, LSB minor
	uint8_t imageScanningTime[4];					// Binary coded seconds since 1970-01-01 GMT
	uint8_t imageModificationTime[4];				// Binary coded seconds since 1970-01-01 GMT
	uint8_t imageMedium;						// 0 - color negative
									// 1 - color reversal
									// 2 - color hard copy
									// 3 - thermal hard copy
									// 4 - black and white negative
									// 5 - black and white reversal
									// 6 - black and white hard copy
									// 7 - internegative
									// 8 - synthetic image
	char productType[20];						// Product code, ISO 646, etc blank padded
	char scannerVendorIdentity[20];					// Vendor ID, ISO 646, etc blank padded
	char scannerProductIdentity[16];				// Scanner ID, ISO 646, etc blank padded
	char scannerFirmwareRevision[4];				// ISO 646, etc blank padded
	char scannerFirmwareDate[8];					// ISO 646, etc blank padded
	char scannerSerialNumber[20];					// ISO 646, etc blank padded
	uint8_t scannerPixelSize[2];					// BCD coded in microns, LSB is after the decimal
	char piwEquipmentManufacturer[20];				// Imaging workstation manufacturer, ISO 646, etc blank padded
	uint8_t photoFinisherCharSet;					// 1 - 38 characters ISO 646
									// 2 - 65 characters ISO 646
									// 3 - 95 characters ISO 646
									// 4 - 191 characters ISO 8859-1
									// 5 - ISO 2022
									// 6 - includes characters not ISO 2375 registered
	char photoFinisherEscapeSequence[32];				// Used for char set 5 and 6, conforms to ISO 2022 excluding esc
									// unused chars 00
	char photoFinisherName[60];					// per previous two fields
	char sbaSignature[3];						// "SBA" - rest of the sba fields exist only if this is defined
	uint8_t sbaRevision[2];						// Binary coded MSB major, LSB minor
	uint8_t sbaCommand;						// 0 - neutral SBA on, color SBA on
									// 1 - neutral SBA off, color SBA off
									// 2 - neutral SBA on, color SBA off
									// 3 - neutral SBA off, color SBA on
	uint8_t sbaData[94];						// Proprietary Kodak data
	uint8_t sbaFTN[2];						// FTN code
	uint8_t sbaData2[4];						// Proprietary Kodak data
	uint8_t copyrightStatus;					// 1 - restrictions apply
									// 0xff - restrictions not specified
									// If restrictions apply, see next field
	char copyrightFile[12];						// Filename of the rights file in the "RIGHTS" directory on the CD
	uint8_t reservedBytes[1192];					// Used fill to 1536
};


//////////////////////////////////////////////////////////////
//
// File header structures - IP-ICA and IPA areas
// This is the structure of the file header section of the PCD
// file. In as much as we are interested/understand it anyway
//
//////////////////////////////////////////////////////////////

// In the IPA areas
struct ImageComponentAttributes {
	uint8_t	reserved[2];
	uint8_t	attributes;						// Bit 7 unused
									// Bit 6-5 : Huffman code class
									// Bit 4 : IPE present (should always be 0)
									// Bit 3-2 : 00 - Base, 01 - 4Base, 10 - 16Base
									// Bit 0-1 : Rotation 00 - 0deg, 01 - 90CCW, 10 - 180CCW, 11 - 270CCW
	uint8_t	sectorStop4Base[2];					// Sector end of the 4 base data
	uint8_t	sectorStop16Base[2];					// Sector end of the 16 base data
	uint8_t	sectorStopIPE[2];					// Should be 0 as IPE is unimplemented
	uint8_t	interleaveRatio;					// Should be 1 for photo CD images only
									// if anything else, we have audio data interleaved.
	uint8_t	ADPCMResolution;
	uint8_t	ADPCMMagnificationPanning[2];
	uint8_t	ADPCMMagnifacationFactor;
	uint8_t	ADPCMDisplayOffset[2];
	uint8_t	ADPCMTransitionDescriptor;
	uint8_t	reserved2[495];						// Fill to 512 bytes
};

// IP-ICA area
struct PCDFileHeader {
	char	signature[7];						// PCD_OPA is overview file
	uint8_t	reserved[2041];						// Fill to sector size
};

// The ICI for each image (scene) is identical to the base16 image, IF the scene exists.
struct PCDFile {
	struct PCDFileHeader				header;		// Signature only
	struct IPIHeader				ipiHeader;	// Basic Image info
	struct ImageComponentAttributes			iciBase16;	// Image attributes for the base16 image
	struct ImageComponentAttributes			iciBase4;	// Image attributes for the base4 image
	struct ImageComponentAttributes			iciBase;	// Image attributes for the base image
	struct ImageComponentAttributes			ici4Base;	// Image attributes for the 4base image
	struct ImageComponentAttributes			ici16Base;	// Image attributes for the 16base image
};


struct PCDFilmTermData {
	short PC, GC, Medium;
	std::string MediumName;
};


enum class PCDResolution {
	Base_div16=0,		//	128 � 192		0.025 Mpix	0.07 Mb		Preview (index print, thumbnail)
	Base_div4,		//	256 � 384		0.098 Mpix	0.28 Mb		Web
	Base,			//	512 � 768		0.393 Mpix	1.13 Mb		Computer screen, TV, Web
	Base_x4,		//	1024 � 1536		1.573 Mpix	4.50 Mb		HDTV screen
	Base_x16,		//	2048 � 3072		6.291 Mpix	18.00 Mb	Print-out up to ca. 20 x 30 cm
	Base_x64,		//	4096 � 6144		25.166 Mpix	72.00 Mb	Professional print, pre-press, archiving (optional)
	_last
};

const int MaxPCDScenes = static_cast<int>(PCDResolution::_last);

enum class PCDOrientation {
	Rot0,
	Rot90,
	Rot180,
	Rot270,
};

enum class PCDUpResMethod {
	Nearest,
	Iterpolate,
	LumaIterpolate,
};

enum class PCDColorSpace {
	Raw = 0,
	LinearCCIR709,
	sRGB,
	YCC
};

enum class PCDWhiteBalance {
	D65 = 0,		// 6500K
	D50,			// 5000K
};

enum class PCDMetaDataDictionary {
	specificationVersion = 0,
	authoringSoftwareRelease,
	imageMagnificationDescriptor,
	imageScanningTime,
	imageModificationTime,
	imageMedium,
	productType,
	scannerVendorIdentity,
	scannerProductIdentity,
	scannerFirmwareRevision,
	scannerFirmwareDate,
	scannerSerialNumber,
	scannerPixelSize,
	piwEquipmentManufacturer,
	photoFinisherName,
	sbaRevision,
	sbaCommand,
	sbaFilm,
	copyrightStatus,
	copyrightFile,
	compressionClass,
	_last
};

const int MaxPCDMetadata = static_cast<int>(PCDMetaDataDictionary::_last);

enum class PCDMedium {
	ColorNegative = 0,
	ColorReversal,
	ColorHardcopy,
	ThermalHardcopy,
	BlackandwhiteNegative,
	BlackandwhiteReversal,
	BlackandwhiteHardcopy,
	interNegative,
	SyntheticImage,
	Chromogenic
};

//////////////////////////////////////////////////////////////
//
// Huffman decoder structure definitions
// These support classes 1 - 4 encoding
//
//////////////////////////////////////////////////////////////
struct ReadBuffer
{
	uint8_t sbuffer[KSectorSize];
	std::istream *fh;
	unsigned long sum;
	unsigned long bits;
	uint8_t *p;
};

struct hctEntry
{
	uint8_t length;							// (length - 1) in bits, so ranges from 0 - 15; max actual length is 16
	uint8_t codeWord[2];						// 16 bit codeword, left justified
	uint8_t key;							// the actual decoded value
};

struct hctTable
{
	uint8_t entries;						// Number of hctEntries in this table
	struct hctEntry entry[256];					// NOTE: the actual length is 4*entries (+1 for the entire structure)
									// 256 is the maximum value
									// The EPT descriptor is after the last actual entry
									// Bits 2:0 define which tables exist
									// Then the actual EPT tables. But EPT never actually seems to have
									// been implemented...........
};

struct huffTable
{
	uint8_t key[0x10000];
	uint8_t len[0x10000];
};


struct huffTables {
	struct huffTable ht[3];
};

#define kHuffmanErrorLen 0x1f

// Vector types
typedef int32_t v4si __attribute__ ((vector_size (16)));
typedef uint32_t v4i __attribute__ ((vector_size (16)));
typedef float v4f __attribute__ ((vector_size (16)));
