/* =======================================================
 * pcdDecode - a Photo-CD image decoder and converter
 * =======================================================
 *
 * Project Info:  http://sourceforge.net/projects/pcdtojpeg/
 * Project Lead:  Sandy McGuffog (sandy.cornerfix@gmail.com);
 *
 * (C) Copyright 2009-2011, by Sandy McGuffog and Contributors.
 *
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * ---------------
 * pcdDecode is written to provide a PCD decoder library that is:
 * 1. Easy to use, with a clear API, and human readable messages
 * 2. Suited to modern multi-core processors; so multithreaded where applicable
 * 3. Multi-platform - OS X, Windows, *nix, little- or big-endian
 * 4. Minimum dependencies - pcdDecode will use OpenMP if available, but is
 *    otherwise standalone (other than for normal C++ libraries)
 * 5. Correctly converts all forms of PCD files, including all classes of huffman
 *    encoding (classes 1-4)
 * 6. Converts all PCD resolutions, including 64Base images
 * 7. Correct and predictable color space handling (no blown highlights, no color casts);
 *    RGB data can be returned in a choice of well specified color spaces.
 * 8. Full support for "Kodak standard" bilinear interpolation of chroma
 * 9. Extracts PCD file metadata information
 * 10. Transparently recovers from many data errors
 * 11. GPL licensed
 *
 * pcdDecode is also available is non-GPL version that adds AHD style interpolation;
 * this offers lower noise and better edge performance.
 *
 * ---------------
 * Compiling: pcdDecode should compile without modification on most systems. It has been
 * tested on OS X (Xcode), MS Windows (Visual C++) and linux (GCC), on both big- and
 * little-endian CPUs
 *
 * ---------------
 * pcdDecode.h
 * ---------------
 * (C) Copyright 2009-2011, by Sandy McGuffog and Contributors.
 *
 * Original Author:  Sandy McGuffog;
 * Contributor(s):   -;
 *
 * Acknowlegements:   Hadmut Danisch (danisch@ira.uka.de), who authored hpcdtoppm in the early
 *                    90's. Although this software itself is unrelated to hpcdtoppm, the pcdDecoder
 *                    package would not have been possible without Hadmut's original reverse
 *                    engineering of the PCD file format.
 *
 *                    Ted Felix (http://tedfelix.com/ ), who provided sample PCD images and assorted
 *                    PCD documentation, as well as testing early versions of this software
 *
 * Changes
 * -------
 *
 *
 * V1.0.2 - 1 July 2009 - Added YCC color space to the decoder library
 *                        Improved sRGB shadow detail and color handling
 * V1.0.3 - 25 Aug 2009 - Fixed a potential string overrun problem
 *
 * V1.0.6 - 12 Jan 2010 - Fix for compile problems under Ubuntu
 *
 * V1.0.8 - 20 Mar 2010 - Fix bug in the conversion of 64base images that
 *                        have a non-native aspect ratio
 *
 * V1.0.9 - 23 Sept 2010 - Fix bug in the conversion of 64base images that
 *                        have long sequence lengths; modifications to allow
 *                        easy compilation as a DLL
 *
 * V1.0.10 - 28 March 2011 - Work around for a bug in GCC that incorrectly handles
 *                           nested structures on the stack
 *
 */


#include <string>
// <filesystem> is currently experimental in stdc++ 7?
//#include <experimental/filesystem>
//namespace fs = std::experimental::filesystem;
#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

#include <ctype.h>
#include <stddef.h>

#ifndef __pcdDecodeIncluded
#define __pcdDecodeIncluded 1

#include "misc.hh"

class pcdDecode
{
public:
	//////////////////////////////////////////////////////////////
	//
	// Class initialiser
	//
	//////////////////////////////////////////////////////////////
	pcdDecode ();

	~pcdDecode ();

	//////////////////////////////////////////////////////////////
	//
	// Header parser
	//
	//////////////////////////////////////////////////////////////
	// in_file : PCD file location
	//
	// return true if image data at any resolution could be read (see getErrorString)
	// When this function returns, metadata and image size is available, but no pixel data.
	bool parseHeader (const fs::path in_file);

	//////////////////////////////////////////////////////////////
	//
	// Read image data
	//
	//////////////////////////////////////////////////////////////
	// ipe_file :  IPE (64Base) file location, NULL for none
	// maxScene : Maximum resolution to decode; member of PCDResolutions
	//
	// Note that the actual decoded resolution may be lower than requested if there
	// is a file error or the file doesn't have the requsted resolution
	// The file not having the requested resolution is NOT regarded as an error; the best
	// available resolution is returned
	bool readImage(const fs::path ipe_file, PCDResolution maxScene);

	//////////////////////////////////////////////////////////////
	//
	// Post parser
	//
	//////////////////////////////////////////////////////////////
	// This assembles the various image deltas into a coherent YCC image
	// To get image data, you must call populateBuffers
	// Multithreaded on platforms that support threading
	void postParse(std::function<void(PCDResolution res, bool)> scene_cb = nullptr);

	//////////////////////////////////////////////////////////////
	//
	// get Width
	//
	//////////////////////////////////////////////////////////////
	// Returns the actual image width; note this is after the image has been rotated
	// to the normal
	size_t getWidth();

	//////////////////////////////////////////////////////////////
	//
	// get Height
	//
	//////////////////////////////////////////////////////////////
	// Returns the actual image height; note this is after the image has been rotated
	// to the normal
	size_t getHeight();

	//////////////////////////////////////////////////////////////
	//
	// Is Mononchrome?
	//
	//////////////////////////////////////////////////////////////
	// Returns true if the image is monochrome
	// This can be either because setIsMonochrome was called, or because the image has only
	// Y data at the resolution requested. Note that color data may exist at a lower resolution.
	bool isMonochrome();

	//////////////////////////////////////////////////////////////
	//
	// Set Mononchrome
	//
	//////////////////////////////////////////////////////////////
	// If this set to true, the images will be procssed as monochrome - i.e. the
	// chroma data will be ignored. Note that values returned from the populateBuffers
	// are still three component RGB, and that those three compoenets may not be equal.
	// The relationship between them depends on the white balance setting
	void setIsMonoChrome(bool val);

	//////////////////////////////////////////////////////////////
	//
	// Get Orientation
	//
	//////////////////////////////////////////////////////////////
	// Returns the orientation of the original PCD image
	// Note that the RGB image returned by the decoder is rotated to the "correct"
	// orientation as indicated by this setting.  i.e. the returned RGB image is always
	// orientation 0.
	// 0 - 0deg, 1 - 90CCW, 2 - 180CCW, 3 - 270CCW
	PCDOrientation getOrientation();

	// Get current scene resolution
	PCDResolution getScene(void);

	// Get maximum resolution stored in the file
	PCDResolution getResolution(void);

	//////////////////////////////////////////////////////////////
	//
	// Digitisation Time
	//
	//////////////////////////////////////////////////////////////
	// Returns digitisation time in seconds since 1/1/1970
	long digitisationTime();

	//////////////////////////////////////////////////////////////
	//
	// Set Interpolation
	//
	//////////////////////////////////////////////////////////////
	// Sets the interpolation to one of the values in PCDUpResMethod:
	// Nearest - nearest neighbour
	// Iterpolate - bilinear intepolation
	// LumaIterpolate - AHD type adaptive intepolation; this gives lower noise
	// and substantially reduces edge artifacts
	// LumaIterpolate is only available in the restricted (non-GPL)
	// version of the decoder
	void setInterpolation(PCDUpResMethod value);

	//////////////////////////////////////////////////////////////
	//
	// Set ColorSpace
	//
	//////////////////////////////////////////////////////////////
	// Sets the color space that RGB data will be returned in. It must be one of
	// the values in enum PCDColorSpaces
	// Raw - Raw PCD data; converted to RGB, but still compressed, etc
	// LinearCCIR709 - a CCIR709 linear light (gamma 1) space
	// sRGB - sRGB space (aka with sRBG primaries and sRGB gamma curve)
	void setColorSpace(PCDColorSpace value);

	//////////////////////////////////////////////////////////////
	//
	// Get ColorSpace
	//
	//////////////////////////////////////////////////////////////
	// Gets the color space as set by setColorSpace
	PCDColorSpace getColorSpace();

	//////////////////////////////////////////////////////////////
	//
	// Set WhiteBalance
	//
	//////////////////////////////////////////////////////////////
	// Sets the white balance for the CCIR709 and sRGB color spaces
	// Must be one of PCDWhiteBalance:
	// D65 - 6500K
	// D50 - 5000K
	// The default (and what PCD images should be scanned at!) is 6500K
	void setWhiteBalance(PCDWhiteBalance value);

	//////////////////////////////////////////////////////////////
	//
	// Get Error String
	//
	//////////////////////////////////////////////////////////////
	// Returns an error string which contains an English language
	// error description, or an empty string, if no error
	// information is available. The error string is set by the parseFile function.
	// If parseFile return false, then the contents of the error string consitute
	// an error message, and no image data is available. If parseFile returns true
	// then the contents of the error string consitute a warning.
	std::string getErrorString();

	//////////////////////////////////////////////////////////////
	//
	// Get Film Term Data
	//
	//////////////////////////////////////////////////////////////
	// Returns the Film Term Number, and the PC and GC values for the medium that
	// was scanned. See Kodak Document PCD067 for more information
	// http://www.kodak.com/global/en/professional/products/storage/pcd/techInfo/pcd067.jhtml
	// A GC value of -1 indicates that no GC value exists.
	// If no film term data is available, FTN, PC and GC are set to 0.
	void getFilmTermData(int& FTN, int& PC, int& GC);

	//////////////////////////////////////////////////////////////
	//
	// Populate RGB buffers
	//
	//////////////////////////////////////////////////////////////
	// Populates the supplied buffers with RGB data.
	// Alpha is always set to 100%; if your application does not require
	// an alpha value, pass NULL for alpha. step is the pointer increment value
	// for the buffers. This allows the use of either interleaved or separate RGB buffers.
	// This function can only be called if parseFile returned true, and
	// postParse has been called.
	// Multithreaded on platforms that support threading
	template <typename T>
	void populateBuffers(T *red, T *green, T *blue, T *alpha, int step, std::function<void(size_t)> row_cb = nullptr);


	//////////////////////////////////////////////////////////////
	//
	// Get Metadata
	//
	//////////////////////////////////////////////////////////////
	// Returns image metadata in English language human readable form.
	// Index is a member of PCDMetaDataDictionary, and must be between
	// 0 and MaxPCDMetadata
	void getMetadata(PCDMetaDataDictionary select, std::string& description, std::string& value);

protected:

	PCDUpResMethod upResMethod;
	bool monochrome;
	uint8_t *luma;
	uint8_t *chroma1;
	uint8_t *chroma2;
	uint8_t *deltas[3][3];
	PCDOrientation imageRotate;
	PCDResolution imageResolution;
	PCDColorSpace colorSpace;
	PCDWhiteBalance whiteBalance;
	size_t imageIPEAvailable;
	size_t imageHuffmanClass;
	PCDResolution baseScene;
	PCDResolution scene;
	uint16_t ipeLayers;
	uint16_t ipeFiles;
	PCDFile *pcdFile;
	std::string errorString;
	std::ifstream fh;

	// Last two elements of these tables are calculated in parseHeader()
	int ICDOffset[MaxPCDScenes] = {4, 23, 96, 389, 0, 0};
	int HCTOffset[MaxPCDScenes] = {0, 0, 0, 388, 0, 0};

	void interpolateBuffers(uint8_t  **c1UpRes, uint8_t **c2UpRes, int *resFactor);
	bool parseICFile (const fs::path ipe_file);
	void pcdFreeAll(void);
};

template <typename T>
void pcdDecode::populateBuffers(T *red, T *green, T *blue, T *alpha, int step, std::function<void(size_t)> row_cb)
{
	uint8_t *lp, *c1p, *c2p, *c1UpRes, *c2UpRes;
	lp = luma;
	c1p = chroma1;
	c2p = chroma2;
	c1UpRes = NULL;
	c2UpRes = NULL;
	PCDSceneMetadata sceneMeta;
	try {
		sceneMeta = PCDScenes.at(scene);
	}
	catch (...) {
		return;
	}
	int resFactor = 1;

	if (pcdFile == NULL)
		return;

#ifdef DEBUG
//	dumpColumn(lp, 356, sceneMeta.lumaHeight, sceneMeta.lumaWidth);
//	dump8by8(c1p, sceneMeta.chromaWidth);
#endif

	interpolateBuffers(&c1UpRes, &c2UpRes, &resFactor);
	if (c1UpRes != NULL)
		c1p = c1UpRes;
	if (c2UpRes != NULL)
		c2p = c2UpRes;

#ifdef DEBUG
//	dump8by8(c1p, sceneMeta.lumaWidth);
#endif

#ifdef __PerformanceAnalysis
#ifdef qMacOS
	AbsoluteTime nowTime, bgnTime;
	bgnTime = UpTime();
#endif
#endif
	convertToRGB<T>(red, green, blue, alpha,
			step, sceneMeta.lumaWidth, sceneMeta.lumaHeight,
			lp, monochrome ? NULL : c1p, monochrome ? NULL : c2p,
			resFactor, imageRotate, colorSpace, whiteBalance, row_cb);

#ifdef __PerformanceAnalysis
#ifdef qMacOS
	nowTime = UpTime();
	float uSec  = HowLong(nowTime, bgnTime);
	fprintf(stderr, " ConvertRGB: %.3f usec \n", uSec);
#endif
#endif
	if (c1UpRes != NULL) {
		free(c1UpRes);
		c1UpRes = NULL;
	}
	if (c2UpRes != NULL) {
		free(c2UpRes);
		c2UpRes = NULL;
	}
}

#endif
