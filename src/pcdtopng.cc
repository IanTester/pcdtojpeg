/* =======================================================
 * pcdtopng - a Photo-CD to PNG image format converter
 * =======================================================
 *
 * Project Info:  http://sourceforge.net/projects/pcdtojpeg/
 * Project Lead:  Sandy McGuffog (sandy.cornerfix@gmail.com);
 *
 * (C) Copyright 2009-2011, by Sandy McGuffog and Contributors.
 *
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * ---------------
 * main.cpp
 * ---------------
 * (C) Copyright 2009-2013, by Sandy McGuffog and Contributors.
 *
 * Original Author:  Sandy McGuffog;
 * Contributor(s):   -;
 *
 * Acknowlegements:   Hadmut Danisch (danisch@ira.uka.de), who authored hpcdtoppm in the early
 *                    90's. Although this software itself is unrelated to hpcdtoppm, the pcdDecoder
 *                    package would not have been possible without Hadmut's original reverse
 *                    engineering of the PCD file format.
 *
 *                    Ted Felix (http://tedfelix.com/ ), who provided sample PCD images and assorted
 *                    PCD documentation, as well as testing early versions of this software
 *
 * Changes
 * -------
 *
 * V1.0.2 - 1 July 2009 - Added YCC color space to the decoder library
 *                        Improved sRGB shadow detail and color handling
 *
 * V1.0.3 - 1 Sept 2009 - V1.0.3 decoder library
 *                        Even more path separator finding code
 *                        Enhanced 64Base IPE file location algorithm
 *                        More descriptive error messages
 * V1.0.4 - 19 Sept 2009 - Multithreading under Windows
 *
 * V1.0.5 - 21 Sept 2009 - More efficient memory management
 *
 * V1.0.7 - 12 Feb 2010 - New decoder library version
 *
 * V1.0.8 - 20 Mar 2010 - New decoder library version
 *
 * V1.0.9 - 23 Sept 2010 - New decoder library version
 *
 * V1.0.10 - 3 April 2011 - New decoder library version
 *
 * V1.0.11 - 3 Jan 2013 - Clean some includes for linux
 *
 * V1.0.12 - 24 April 2015 - New decoder library version, clean up includes
 *
 */

#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include <png.h>
#include <zlib.h>
#include <lcms2.h>
#include "pcdDecode.hh"

#if defined(_WIN32) || defined(_WIN32_) || defined(__WIN32__) || defined(WIN32) || defined(_MSC_VER) || defined(__CYGWIN__) || defined(__MINGW32__) || defined(__BORLANDC__)
#define pcdHaveWinOS 1
#include <direct.h>
#else
#include <unistd.h>
#endif


//! libPNG callback for writing to an ostream
void png_write_ostream_cb(png_structp png, png_bytep buffer, png_size_t length) {
	std::ostream *os = (std::ostream*)png_get_io_ptr(png);
	os->write((char*)buffer, length);
}

//! libPNG callback for flushing an ostream
void png_flush_ostream_cb(png_structp png) {
	std::ostream *os = (std::ostream*)png_get_io_ptr(png);
	os->flush();
}

//////////////////////////////////////////////////////////////
//
// PNG output
//
//////////////////////////////////////////////////////////////

void write_PNG_file (fs::path filename,
		     png_byte * image_buffer,
		     int image_height,
		     int image_width,
		     uint8_t depth)
{
	std::ofstream outfile;
	// Specify data destination (our file)
	outfile.open(filename.native(), std::ios_base::out | std::ios_base::binary);
	if (outfile.fail()) {
		fprintf(stderr, "can't open %s\n", filename.c_str());
		exit(1);
	}

	png_structp png = png_create_write_struct(PNG_LIBPNG_VER_STRING,
				      NULL, NULL, NULL);
	if (!png) {
		fprintf(stderr, "Could not create PNG write structure.\n");
		exit(1);
	}

	png_infop info = png_create_info_struct(png);
	if (!info) {
		png_destroy_write_struct(&png, (png_infopp)NULL);
		fprintf(stderr, "Could not create PNG info structure.\n");
		exit(1);
	}

	if (setjmp(png_jmpbuf(png))) {
		png_destroy_write_struct(&png, &info);
		outfile.close();
		fprintf(stderr, "Something went wrong writing the PNG file.\n");
		exit(1);
	}

	png_set_write_fn(png, &outfile, png_write_ostream_cb, png_flush_ostream_cb);

	png_set_IHDR(png, info,
		     image_width, image_height, depth, PNG_COLOR_TYPE_RGB,
		     PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

	png_set_filter(png, 0, PNG_ALL_FILTERS);
	png_set_compression_level(png, Z_BEST_COMPRESSION);


	cmsHPROFILE sRGB = cmsCreate_sRGBProfile();
	cmsUInt32Number sRGB_size;
	if (cmsSaveProfileToMem(sRGB, NULL, &sRGB_size)) {
		void *sRGB_data = malloc(sRGB_size);
		if (sRGB_data != nullptr) {
			if (cmsSaveProfileToMem(sRGB, sRGB_data, &sRGB_size)) {
#if PNG_LIBPNG_VER < 10500
				png_set_iCCP(png, info, "sRGB", 0, (png_charp)sRGB_data, sRGB_size);
#else
				png_set_iCCP(png, info, "sRGB", 0, (png_const_bytep)sRGB_data, sRGB_size);
#endif
			}
			free(sRGB_data);
		}
	}

	{
		time_t t = time(NULL);
		if (t > 0) {
			png_time ptime;
			png_convert_from_time_t(&ptime, t);
			png_set_tIME(png, info, &ptime);
		}
	}

	png_write_info(png, info);
	png_set_swap(png);

	for (int y = 0; y < image_height; y++) {
	  png_write_row(png, image_buffer + (y * image_width * 3 * (depth >> 3)));
	}

	png_write_end(png, info);

	png_destroy_write_struct(&png, &info);
	outfile.close();
}


//////////////////////////////////////////////////////////////
//
// The main program
//
//////////////////////////////////////////////////////////////
// All this does is to parse the command line, pass the file to the decoder,
// apply brightness if requested, then write out the file as a PNG

#define kpcdtopngVersion "1.0.12"

void printName()
{
	fprintf (stderr,
			 "\n"
			 "pcdtopng, version " kpcdtopngVersion " "
			 "\n"
			 "Copyright (C) 2009-2015 Sandy McGuffog\n");
}

void printUsage(char * const argv [], bool isVerbose)
{
	if (!isVerbose) printName();
	fprintf (stderr,
			 "\n"
			 "Usage:  %s [options] file1 [file2]\n"
			 "\n"
			 "Valid options:\n"
			 "-h            Print this message\n"
			 "-v            Verbose file information\n"
			 "-m            Process the file as monochrome\n"
			 "-D50          Process for a white balance of D50\n"
			 "-D65          Process for a white balance of D65 <default>\n"
			 "-B		Produce 16-bit components instead of 8-bit\n"
			 "-r n          Highest resolution to extract (n range 0 to 5):\n"
			 "                 0 - Base/16 (128 x 192)\n"
			 "                 1 - Base/4 (256 x 384)\n"
			 "                 2 - Base (512 x 768)\n"
			 "                 3 - 4Base (1024 x 1536)\n"
			 "                <4 - 16Base (2048 x 3072)>\n"
			 "                 5 - 64Base (4096 x 6144)\n"
			 "\n",
			 argv [0]);
}

int main (int argc, char * const argv[]) {
    // parse the arguments
	bool doneWithArguments = false;
	int argIndex = 1;
	bool isVerbose = false;
	bool isMonochrome = false;
	bool isD50White = false;
	bool is16Bit = false;
	float boost = 0.0f;
	PCDResolution resolution = PCDResolution::Base_x16;
	size_t width, height;
	pcdDecode *decoder;
	fs::path outFile, iceFile;

	if (argc < 2) {
		printUsage(argv, isVerbose);
		exit(0);
	}
	while ((!doneWithArguments) && (argIndex < argc)) {
		std::string thisArg(argv[argIndex]);
		if (thisArg[0] == '-') {
			if ((thisArg == "-h") || (thisArg == "-H")) {
				printUsage(argv, isVerbose);
				exit(0);
			}
			else if (thisArg == "-v") {
				isVerbose = true;
				printName();
			}
			else if (thisArg == "-m") {
				isMonochrome = true;
			}
			else if (thisArg == "-D50") {
				isD50White = true;
			}
			else if (thisArg == "-D65") {
				isD50White = false;
			}
			else if (thisArg == "-B") {
				is16Bit = true;
			}
			else if (thisArg == "-b") {
				if (argIndex > (argc - 2)) {
					printUsage(argv, isVerbose);
					exit(-1);
				}
				boost = (float) atof(argv[argIndex+1]);
				if (boost < -0.5)
					boost = -0.5;
				else if (boost > 7)
					boost = 7;
				argIndex++;
			}
			else if (thisArg == "-r") {
				if (argIndex > (argc - 2)) {
					printUsage(argv, isVerbose);
					exit(-1);
				}
				int r = atoi(argv[argIndex+1]);
				if (r < 0)
				  r = 0;
				if (r > 5)
				  r = 5;
				resolution = static_cast<PCDResolution>(r);
				argIndex++;
			}
			else {
				fprintf (stderr, "Invalid argument\n");
				printUsage(argv, isVerbose);
				exit(-1);
				}
			argIndex++;
		}
		else {
			doneWithArguments = true;
		}
	}
	// Now check we have file[s]
	if (argIndex > (argc - 1)) {
		fprintf (stderr, "Invalid argument\n");
		printUsage(argv, isVerbose);
		exit(-1);
	}

	// Let's see if we can actually find this file
	// Of course, windows can't even have a normal stat function.....

#if defined(pcdHaveWinOS)
	struct _stat stFileInfo;
	if (_stat(argv[argIndex], &stFileInfo) == -1) {
#else
	struct stat stFileInfo;
	if (stat(argv[argIndex], &stFileInfo) == -1) {
#endif
		fprintf (stderr, "pcdtopng could not find the file \"%s\" - check the name you entered\n", argv[argIndex]);
		exit(-1);
	}
	// Get us a decoder
	decoder = new pcdDecode();
	if (decoder == NULL) {
		fprintf (stderr, "Could not create a decoder - probably too little memory\n");
		exit(-1);
	}
	// Set to the best possible quality interpolation; if this is the the GPL decoder,
	// it doesn't actually have the LumaIterpolate, but will automatically fall
	// back to the best it has
	decoder->setInterpolation(PCDUpResMethod::LumaIterpolate);

	if (resolution > PCDResolution::Base_x16) {
		// We need to generate the location of the IC file.
		// Note this only works if the directory structure is the same as the original CD
		// Also note that here, if there isn't a path separator in the path, we can't find
		// the file anyway, so this is an ok way to decide what separator to use.
		iceFile = fs::absolute(argv[argIndex]);

		std::string nameOnly = iceFile.filename().native();
		size_t loc = nameOnly.find_last_of(".");
		if (loc != std::string::npos)
			nameOnly.erase(loc);
		bool useLowerCase = (nameOnly.find_last_of("img") != std::string::npos);

		iceFile = iceFile.parent_path()
		  / (useLowerCase ? "ipe" : "IPE")
		  / nameOnly
		  / (useLowerCase ? "64base" : "64BASE")
		  / (useLowerCase ? "info.ic" : "INFO.IC");
	}

	// If we want monochrome, now is the time to ask for it
	decoder->setIsMonoChrome(isMonochrome);

	// If we want D50, now is the time to ask for it
	decoder->setWhiteBalance(isD50White ? PCDWhiteBalance::D50 : PCDWhiteBalance::D65);

	// Parse the file header
	if (!decoder->parseHeader(argv[argIndex])) {
		// false here means there isn't any kind of a valid image
		fprintf (stderr, "Decoder Error: %s\n", decoder->getErrorString().c_str());
		delete (decoder);
		if (resolution > PCDResolution::Base_x16) {
			fprintf (stderr, " while trying to process ICE file \"%s\"\n", iceFile.c_str());
		}
		exit(-1);
	}

	// Read the image data
	if (!decoder->readImage(iceFile, resolution)) {
		// false here means there isn't any kind of a valid image
		fprintf (stderr, "Decoder Error: %s\n", decoder->getErrorString().c_str());
		delete (decoder);
		if (resolution > PCDResolution::Base_x16) {
			fprintf (stderr, " while trying to process ICE file \"%s\"\n", iceFile.c_str());
		}
		exit(-1);
	}

	// Even if we got a valid image back, there may be warnings - let's print those:
	if (decoder->getErrorString().length() > 0) {
		fprintf (stderr, "Warning: %s\n", decoder->getErrorString().c_str());
		if (resolution > PCDResolution::Base_x16) {
			fprintf (stderr, " while trying to process ICE file \"%s\"\n", iceFile.c_str());
		}
	}

	// At this point we have all the metadata, so we can take decisions based on that
	// (e.g., resolution, original medium) if that's what we want
	// But the data is still in its component pieces as in the original file, so we can't
	// call any of the populateBuffer routines
	if (isVerbose) {
		std::string descrip, val;
		printf("Image metadata:\n");
		for (int i = 0; i < MaxPCDMetadata; i++) {
			decoder->getMetadata(static_cast<PCDMetaDataDictionary>(i), descrip, val);
			printf("  %s: %s\n", descrip.c_str(), val.c_str());
		}
	}

	// We might not actually have gotten an image at the resolution we asked for,
	// so get the size of what we have got.....
	width = decoder->getWidth();
	height = decoder->getHeight();
	if (isVerbose) {
		printf("Image size: %d x %d\n", (int) decoder->getWidth(), (int) decoder->getHeight());
	}

	// Now we post parse. This assembles all the various pieces of base and residual
	// image data into a single YCC format image. This operation is multi-threaded,
	// if multi-threading is enabled in the decoder library
	decoder->postParse();

	// sRGB is by far the most widely accepted color space, so set up for that
	decoder->setColorSpace(PCDColorSpace::sRGB);

	setup_boost(boost);

	png_byte *png_image_data = nullptr;
	if (is16Bit) {
		uint16_t *pixels = new uint16_t[width * height * 3];
		if (pixels != NULL) {
			decoder->populateBuffers<uint16_t>(&(pixels[0]), &(pixels[1]), &(pixels[2]), NULL, 3);
			png_image_data = (png_byte*)pixels;
		}
		else {
			fprintf (stderr, "Could not allocate memory for the PNG conversion\n");
			delete (decoder);
			exit(-1);
		}
	} else {
		uint8_t *pixels = new uint8_t[width * height * 3];
		if (pixels != NULL) {
			decoder->populateBuffers<uint8_t>(&(pixels[0]), &(pixels[1]), &(pixels[2]), NULL, 3);
			png_image_data = (png_byte*)pixels;
		}
		else {
			fprintf (stderr, "Could not allocate memory for the PNG conversion\n");
			delete (decoder);
			exit(-1);
		}
	}
	// We can free the decoder's memory now, as we have the buffer full of RGB data
	delete (decoder);
	decoder = NULL;

	// If an output file wasn't specified, synthesize a filename
	if (argIndex < (argc-1)) {
		outFile = argv[argIndex+1];
	}
	else {
		// Create a new filename
		std::string baseFile(argv[argIndex]);
		size_t loc = baseFile.find_last_of(".");
		// Make sure there is enough room in the string, because the standard
		// C++ string implementation is too dumb to do so.....
		baseFile.resize(baseFile.size()+3);
		if (loc != std::string::npos) {
			baseFile = baseFile.erase(loc);
		}
		baseFile = baseFile.append(".png");
		outFile = baseFile;
	}

	// Now we just compress the buffer into a PNG format file and also add the sRGB profile.
	// If we don't add the profile, then all our hard work in the decoder to keep the
	// color space straight goes to waste.....
	write_PNG_file (outFile,
			png_image_data,
			(int) height,
			(int) width,
			is16Bit ? 16 : 8);

	delete [] png_image_data;

	return 0;
}
